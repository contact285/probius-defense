# What is Corditia?
Corditia is a third-person tower defense/base building game currently being developed with Unity 2020.

# How can I download/play Corditia?
When the game is ready for release, you'll find a link to its Itch.io page here. Stay tuned!

# What else is there to know?
I sometimes livestream my work on Corditia. You can find me here: https://www.twitch.tv/seebeesee