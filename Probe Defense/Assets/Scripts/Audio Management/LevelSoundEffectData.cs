﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SfxData
{
    // Name of this data point
    public string sfxName;
    // ID used to reference this specific data point
    public string sfxID;
    // Clip played when used
    public AudioClip sfxClip;
    // Volume this clip is played at
    public float sfxVolume;
    // Maximum distance for attenuation/audibility
    public float sfxMaxDistance;
}

[CreateAssetMenu(fileName = "ResourceData", menuName = "ScriptableObjects/LevelSoundEffectData", order = 3)]
public class LevelSoundEffectData : ScriptableObject
{
    public List<SfxData> data;

    public SfxData GetSfxByID(string requestID)
    {
        foreach(SfxData sfx in data)
        {
            // Will cause problems if duplicate IDs are present- will
            // only return the first one found
            if (sfx.sfxID == requestID) return sfx;
        }
        Debug.LogError("No SFX with ID \"" + requestID + "\" was found.");
        return null;
    }
}
