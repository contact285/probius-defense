﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MasterAudioController : MonoBehaviour
{
    public static MasterAudioController mac { get; private set; }

    public LevelSoundEffectData levelSFX;
    public int startingPoolCount = 0;
    public GameObject blankAudioSourcePrefab;

    public int inactive = 0;
    public int active = 0;
    public int totalSources = 0;

    // Pool of inactive audio sources
    [SerializeField] List<GameObject> inactiveSFXSources = new List<GameObject>();
    // Pool of actively playing audio sources
    [SerializeField] List<GameObject> activeSFXSources = new List<GameObject>();

    private void Awake()
    {
        if (mac != null)
        {
            Destroy(mac.gameObject);
        }
        mac = this;
        MakeBaseSFXPool();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        CheckActiveSFXPool();
        active = activeSFXSources.Count;
        inactive = inactiveSFXSources.Count;
        totalSources = active + inactive;
    }

    /// <summary>
    /// Utilizes a specified int startingPoolCount to create inactive audio sources at the start of the level
    /// </summary>
    void MakeBaseSFXPool()
    {
        for (int i=0; i< startingPoolCount; i++)
        {
            var newSource = Instantiate(blankAudioSourcePrefab, this.transform);
            // Set new audio source to inactive by default
            newSource.SetActive(false);
            inactiveSFXSources.Add(newSource);
        }
    }

    void CheckActiveSFXPool()
    {
        List<GameObject> sfxToMakeInactive = new List<GameObject>();
        foreach(GameObject sfx in activeSFXSources)
        {
            // if the audiosource on an sfx object has stopped playing
            //  set it to inactive
            //  move it to list of sfxtoMakeInactive
            if (sfx.GetComponent<AudioSource>().isPlaying == false)
            {
                sfx.SetActive(false);
                sfxToMakeInactive.Add(sfx);
            }
        }
        // for each obj in makeInactive list
        // remove from active list, and add to inactive
        foreach(GameObject sfx in sfxToMakeInactive)
        {
            activeSFXSources.Remove(sfx);
            inactiveSFXSources.Add(sfx);
        }
    }

    public void PlaySfxByID(Vector3 playLocation, string requestID)
    {
        var newSFXData = levelSFX.GetSfxByID(requestID);
        if (newSFXData != null)
        {
            // Check list of inactiveSFX to see if one is available
            //  if the inactive list is empty
            //      instantiate a new sfx and add it to the inactive list
            //  if the inactive list is NOT empty (which it won't, if we've instantiated), 
            //      set it to active
            //      assign the sfxData clip and properties
            //      move the sfx to the location
            //      remove the sfx from the inactive list and add it to the active list
            if (inactiveSFXSources.Count == 0)
            {
                Debug.LogWarning("Inactive pool is empty, adding 1 new source to pool");
                var newSource = Instantiate(blankAudioSourcePrefab, this.transform);
                // Set new audio source to inactive by default
                newSource.SetActive(false);
                inactiveSFXSources.Add(newSource);
            }

            var sfxToUse = inactiveSFXSources[0];
            if (sfxToUse != null)
            {
                sfxToUse.SetActive(true);
                var sfxSource = sfxToUse.GetComponent<AudioSource>();
                sfxSource.clip = newSFXData.sfxClip;
                sfxSource.volume = newSFXData.sfxVolume;
                sfxSource.maxDistance = newSFXData.sfxMaxDistance;

                sfxToUse.transform.position = playLocation;
                inactiveSFXSources.Remove(sfxToUse);
                activeSFXSources.Add(sfxToUse);

                sfxSource.Play();
            }
        }
    }
}
