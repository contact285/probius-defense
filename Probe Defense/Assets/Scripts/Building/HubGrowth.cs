﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HubGrowth : MonoBehaviour
{
    public bool allowGrowth;
    public Transform AOI;
    public float startingScale;
    public float maxScale;
    public float growthRate;
    public float growthAmount;
    float nextGrowthStage;

    // Start is called before the first frame update
    void Start()
    {
        AOI.localScale = new Vector3(startingScale, startingScale, startingScale);
        nextGrowthStage = startingScale;
        if (allowGrowth == true)
        {
            // Trigger the first growth period in growthRate seconds, and repeat every growthRate seconds
            InvokeRepeating("TriggerAOIGrowth", growthRate, growthRate);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Add to the local scale every step, up to the max
        //if (allowGrowth == true && AOI.localScale.x < maxScale)
        //{
        //    AOI.localScale = new Vector3(
        //        AOI.localScale.x + growthAmount,
        //        AOI.localScale.y + growthAmount,
        //        AOI.localScale.z + growthAmount
        //    );
        //}
    }

    void TriggerAOIGrowth()
    {
        //Add to the local scale every trigger, up to the max
        if (allowGrowth == true && AOI.localScale.x < maxScale)
        {
            AOI.localScale = new Vector3(
                AOI.localScale.x + growthAmount,
                AOI.localScale.y + growthAmount,
                AOI.localScale.z + growthAmount
            );
        }
        else CancelInvoke();
    }
}
