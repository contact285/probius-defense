﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlacementUI : MonoBehaviour
{
    //public PlaceableBuildings buildingData;
    public TMP_Text selectedBuilding;
    public List<Transform> buildCostHolders;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //UpdateSelectedBuilding();
    }

    public void UpdateSelectedBuilding(Building building)
    {
        selectedBuilding.SetText(building.name);

        // For each resource cost UI, if there is a corresponding resource cost for this building,
        // activate and update that UI element. Otherwise, deactivate that UI element.

        //Debug.Log("Updating resource costs");
        for (int i=0; i<buildCostHolders.Count; i++)
        {
            if (i<building.resourceCosts.Count)
            {
                // Activate object
                buildCostHolders[i].gameObject.SetActive(true);
                // Set the text on the UI object
                buildCostHolders[i].GetChild(0).GetComponent<TMP_Text>().text = 
                    "(" + building.resourceCosts[i].resourceCount + ") "
                    + building.resourceCosts[i].resourceName;
                // Set the sprite of the UI object
                buildCostHolders[i].GetComponent<Image>().sprite =
                    ResourceManager.rm.GetSpriteFromResourcePool(building.resourceCosts[i].resourceName);
            }
            else
            {
                //Debug.Log("Deactivating");
                buildCostHolders[i].gameObject.SetActive(false);
            }
        }
    }

}
