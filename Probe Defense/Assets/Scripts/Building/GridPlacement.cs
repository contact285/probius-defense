﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using UnityEngine;
using UnityEngine.InputSystem;

public class GridPlacement : MonoBehaviour
{
    public Canvas BuildCanvas;
    public GameObject previewObj = null;

    int selectedBuildingIndex = 0;
    public float posLerpSpeed = 5f;
    public float canvasVerticalOffset = 2f;
    [Range(0.035f, 2f)]
    public float canvasScale;
    public float snapInterval = 1.0f;
    public float rotationValue = 90;

    bool canBuild = true;

    public PlaceableBuildings buildingData;

    List<string> availableBuildings;

    public float rotationOffset = 0f;

    // Start is called before the first frame update
    void Start()
    {
        // Disable component if the canvas is not assigned
        if (BuildCanvas == null)
        {
            Debug.LogWarning("BuildCanvas is not assigned, disabling Object Placement.");
            enabled = false;
        }
        //Debug.Log(buildingData.FindBuildingByName("TestTurret").health);
        availableBuildings = buildingData.GetBuildingNames();
        BuildCanvas.GetComponent<PlacementUI>().UpdateSelectedBuilding(buildingData.FindBuildingByName(availableBuildings[selectedBuildingIndex]));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlaceSelectedBuilding()
    {
        if (previewObj != null && canBuild 
            && ResourceManager.rm.CheckResourceCost(buildingData.FindBuildingByName(availableBuildings[selectedBuildingIndex]).resourceCosts))
        {
            // Add conditions to allow placement?
            ResourceManager.rm.ConsumeFromResourceCosts(buildingData.FindBuildingByName(availableBuildings[selectedBuildingIndex]).resourceCosts);

            GameObject newBuilding;
            if (buildingData.FindBuildingByName(availableBuildings[selectedBuildingIndex]).prefab.tag == "Production")
            {
                RaycastHit nodeHit;
                Physics.Raycast(previewObj.GetComponent<Collider>().bounds.center, -previewObj.transform.up, out nodeHit);
                //Debug.Log(nodeHit.collider.name);
                newBuilding = Instantiate(buildingData.FindBuildingByName(availableBuildings[selectedBuildingIndex]).prefab);
                newBuilding.transform.position = previewObj.transform.position;
                newBuilding.transform.rotation = previewObj.transform.rotation;

                // Play construction sound at source of building
                MasterAudioController.mac.PlaySfxByID(newBuilding.transform.position, "playerConstruct");

                if (nodeHit.collider.tag == "Node")
                {
                    var nodeScript = nodeHit.transform.GetComponent<Node>();
                    newBuilding.GetComponent<ProductionMachine>().SetNode(nodeScript);
                }
            }
            else
            {
                newBuilding = Instantiate(buildingData.FindBuildingByName(availableBuildings[selectedBuildingIndex]).prefab);
                newBuilding.transform.position = previewObj.transform.position;
                newBuilding.transform.rotation = previewObj.transform.rotation;

                // Play construction sound at source of building
                MasterAudioController.mac.PlaySfxByID(newBuilding.transform.position, "playerConstruct");

                if (newBuilding.tag == "Hub")
                {
                    // Add child AOI object to AOIManager
                    AOIManager.aoim.AddAOI(newBuilding.transform.GetChild(0).gameObject);
                }
                else if (newBuilding.tag == "ContainsHub")
                {
                    // Add first child's (MUST be Hub object) child to AOIManager
                    AOIManager.aoim.AddAOI(newBuilding.transform.GetChild(0).GetChild(0).gameObject);
                }
            }
        }
    }

    public void RotateSelectedBuilding(int direction, bool modified)
    {
        //rotationOffset += (modified ? rotationValue / 3f : rotationValue) * direction;

        previewObj.transform.localRotation = Quaternion.Euler(
            previewObj.transform.localEulerAngles.x,
            previewObj.transform.localEulerAngles.y + (rotationValue * direction),
            previewObj.transform.localEulerAngles.z
        );
    }

    public void ChangeSelectedBuilding(InputAction.CallbackContext ctx)
    {
        if (previewObj != null)
        {
            if (ctx.performed)
            {
                //Debug.Log(ctx.ReadValue<Vector2>());
                var direction = ctx.ReadValue<Vector2>()[1];
                //Debug.Log(direction);

                // Direction is > 0 or < 0 if moving, otherwise 0 when it stops
                if (direction > 0)
                {
                    selectedBuildingIndex++;
                    if (selectedBuildingIndex >= availableBuildings.Count) selectedBuildingIndex = 0;
                }
                else if (direction < 0)
                {
                    selectedBuildingIndex--;
                    if (selectedBuildingIndex < 0) selectedBuildingIndex = availableBuildings.Count - 1;
                }

                //Debug.Log(selectedBuildingIndex);
                //Debug.Log(buildingData.FindBuildingByName(availableBuildings[selectedBuildingIndex]).name);
                Destroy(previewObj);
                previewObj = Instantiate(buildingData.FindBuildingByName(availableBuildings[selectedBuildingIndex]).preview);
                buildingData.SelectBuilding(availableBuildings[selectedBuildingIndex]);
                // Update the building UI with the newly-selected building
                BuildCanvas.GetComponent<PlacementUI>().UpdateSelectedBuilding(buildingData.FindBuildingByName(availableBuildings[selectedBuildingIndex]));
            }
        }
    }

    /// <summary>
    /// Update the BuildCanvas to hover over the location the player is looking at, 
    ///  and force it to face the player on its Y axis.
    /// </summary>
    /// <param name="lookPosition"> The position the player is currently looking at. </param>
    /// <param name="player"> The player, which the UI should face. </param>
    /// <param name="distance"> The distance between the lookPosition and the player, used to scale the canvas at close/long distances. </param>
    public void UpdateBuildUITransform(Vector3 lookPosition, Transform player, float distance)
    {
        //BuildCanvas.transform.position = lookPosition;
        BuildCanvas.transform.LookAt(player);
        BuildCanvas.transform.rotation = Quaternion.Euler(
            transform.rotation.eulerAngles.x,
            transform.rotation.eulerAngles.y,
            0);
        Vector3 correctedLookPos = new Vector3(
            lookPosition.x,
            lookPosition.y + canvasVerticalOffset,
            lookPosition.z
            ); ;
        BuildCanvas.transform.position = Vector3.Lerp(BuildCanvas.transform.position, correctedLookPos, Time.deltaTime * posLerpSpeed);

        canvasScale = (distance / (24f)) * (.9f) + 0.1f;
        //Debug.Log(distance + " " + canvasScale);
        BuildCanvas.transform.localScale = new Vector3(canvasScale, canvasScale, canvasScale);


        if (previewObj != null)
        {
            MoveToPoint(lookPosition);
            canBuild = previewObj.GetComponent<BuildPreview>().CheckCanBuild();
        }
        
    }

    public void SetBuildCanvasActive(bool onOff)
    {
        if (BuildCanvas.gameObject.activeSelf != onOff) BuildCanvas.gameObject.SetActive(onOff);
        //if (previewObj != null && previewObj.gameObject.activeSelf != onOff) previewObj.gameObject.SetActive(onOff);

        if (onOff == true && previewObj == null)
        {
            previewObj = Instantiate(buildingData.FindBuildingByName(availableBuildings[selectedBuildingIndex]).preview);
            buildingData.SelectBuilding(availableBuildings[selectedBuildingIndex]);
        }
        else if (onOff == false && previewObj != null)
        {
            Destroy(previewObj);
            previewObj = null;
        }
    }

    void SnapToGrid(Vector3 lookPosition)
    {
        Vector3 snapPos = new Vector3(
            RoundTo(lookPosition.x, snapInterval),
            lookPosition.y+0.1f,
            RoundTo(lookPosition.z, snapInterval)
        );
        previewObj.transform.position = snapPos;
        
    }

    void MoveToPoint(Vector3 lookPosition)
    {
        previewObj.transform.position = lookPosition;
    }

    float RoundTo(float value, float multiple)
    {
        return Mathf.Round(value / multiple) * multiple;
    }

    public string GetBuildingTag()
    {
        return buildingData.FindBuildingByName(availableBuildings[selectedBuildingIndex]).preview.tag;
    }
}
