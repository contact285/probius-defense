﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SalvageUI : MonoBehaviour
{
    public TMP_Text selectedBuilding;
    public List<Transform> salvageReturnHolders;
    // Start is called before the first frame update
    void Start()
    {
        ResetSalvageBuilding();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateSalvageBuilding(Building building)
    {
        selectedBuilding.SetText(building.name);

        // For each resource cost UI, if there is a corresponding resource cost for this building,
        // activate and update that UI element. Otherwise, deactivate that UI element.

        //Debug.Log("Updating resource costs");
        for (int i = 0; i < salvageReturnHolders.Count; i++)
        {
            if (i < building.resourceCosts.Count)
            {
                // Activate object
                salvageReturnHolders[i].gameObject.SetActive(true);
                // Set the text on the UI object
                //Debug.Log(building.resourceCosts[i]);
                salvageReturnHolders[i].GetChild(0).GetComponent<TMP_Text>().text =
                    "(" + building.resourceCosts[i].resourceCount + ") "
                    + building.resourceCosts[i].resourceName;
                // Set the sprite of the UI object
                salvageReturnHolders[i].GetComponent<Image>().sprite =
                    ResourceManager.rm.GetSpriteFromResourcePool(building.resourceCosts[i].resourceName);
            }
            else
            {
                //Debug.Log("Deactivating");
                salvageReturnHolders[i].gameObject.SetActive(false);
            }
        }
    }
    
    void ResetSalvageBuilding()
    {
        selectedBuilding.SetText("Nothing Selected");
        for (int i = 0; i < salvageReturnHolders.Count; i++)
        {
            salvageReturnHolders[i].gameObject.SetActive(false);
        }
    }
}
