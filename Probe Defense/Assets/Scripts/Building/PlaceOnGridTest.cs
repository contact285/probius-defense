﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceOnGridTest : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject testObj;
    public float interval = 1.0f;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        SnapToGrid();
    }

    void SnapToGrid()
    {
        Vector3 currentPos = transform.position;
        Vector3 snapPos = new Vector3(
            RoundTo(currentPos.x, interval),
            RoundTo(currentPos.y, interval),
            RoundTo(currentPos.z, interval)
        );
        testObj.transform.position = snapPos;
    }

    float RoundTo(float value, float multiple)
    {
        return Mathf.Round(value / multiple) * multiple;
    }
}
