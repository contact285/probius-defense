﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeconstructBuildings : MonoBehaviour
{

    public Canvas SalvageCanvas;
    public float posLerpSpeed = 5f;
    public float canvasVerticalOffset = 2f;
    [Range(0.035f, 2f)]
    public float canvasScale;
    public float snapInterval = 1.0f;
    public float rotationValue = 90;

    public PlaceableBuildings buildingData;

    public float rotationOffset = 0f;

    GameObject buildingToSalvage;
    Building btsData;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// Update the BuildCanvas to hover over the location the player is looking at, 
    ///  and force it to face the player on its Y axis.
    /// </summary>
    /// <param name="lookPosition"> The position the player is currently looking at. </param>
    /// <param name="player"> The player, which the UI should face. </param>
    /// <param name="distance"> The distance between the lookPosition and the player, used to scale the canvas at close/long distances. </param>
    public void UpdateSalvageUITransform(Vector3 lookPosition, Transform player, float distance)
    {
        //BuildCanvas.transform.position = lookPosition;
        SalvageCanvas.transform.LookAt(player);
        SalvageCanvas.transform.rotation = Quaternion.Euler(
            transform.rotation.eulerAngles.x,
            transform.rotation.eulerAngles.y,
            0);
        Vector3 correctedLookPos = new Vector3(
            lookPosition.x,
            lookPosition.y + canvasVerticalOffset,
            lookPosition.z
            ); ;
        SalvageCanvas.transform.position = Vector3.Lerp(SalvageCanvas.transform.position, correctedLookPos, Time.deltaTime * posLerpSpeed);

        canvasScale = (distance / (24f)) * (1.1f) + 0.1f;
        //Debug.Log(distance + " " + canvasScale);
        SalvageCanvas.transform.localScale = new Vector3(canvasScale, canvasScale, canvasScale);


        //if (previewObj != null)
        //{
        //    SnapToGrid(lookPosition);
        //    canBuild = !previewObj.GetComponent<BuildPreview>().CheckSpaceOccupied();
        //}

    }

    public void SetSalvageCanvasActive(bool onOff)
    {
        if (SalvageCanvas.gameObject.activeSelf != onOff) SalvageCanvas.gameObject.SetActive(onOff);
        //if (previewObj != null && previewObj.gameObject.activeSelf != onOff) previewObj.gameObject.SetActive(onOff);

        //if (onOff == true && previewObj == null)
        //{
        //    previewObj = Instantiate(buildingData.FindBuildingByName(availableBuildings[selectedBuildingIndex]).preview);
        //    buildingData.SelectBuilding(availableBuildings[selectedBuildingIndex]);
        //}
        //else if (onOff == false && previewObj != null)
        //{
        //    Destroy(previewObj);
        //    previewObj = null;
        //}
    }

    public void SetSalvagedBuilding(GameObject obj)
    {
        //Debug.Log(obj.name);
        // Returns object name without trailing space/(clone)
        var cleanName = obj.name.Split('(')[0].Trim(' ');
        //Debug.Log(cleanName);
        var building = buildingData.FindBuildingByName(cleanName);
        //Debug.Log(building.resourceCosts[0].resourceName);
        SalvageCanvas.GetComponent<SalvageUI>().UpdateSalvageBuilding(building);
        buildingToSalvage = obj;
        btsData = building;
    }

    public void ResetBuildingToSalvage()
    {
        if (buildingToSalvage != null)
        {
            buildingToSalvage = null;
            btsData = null;
        }
    }

    public void SalvageBuilding()
    {
        if (buildingToSalvage != null)
        {
            
            // Salvaged hubs do not return resources
            // (but at the moment, tiles that *contain* hubs still return 100% resources
            if (buildingToSalvage.tag == "Hub")
            {
                // Add child AOI object to AOIManager
                AOIManager.aoim.RemoveAOI(buildingToSalvage.transform.GetChild(0).gameObject);
            }
            else if (buildingToSalvage.tag == "ContainsHub")
            {
                // Add first child's (MUST be Hub object) child to AOIManager
                AOIManager.aoim.RemoveAOI(buildingToSalvage.transform.GetChild(0).GetChild(0).gameObject);

                // Return resources for the salvaged building
                foreach (ResourceCost rc in btsData.resourceCosts)
                {
                    ResourceManager.rm.AddResource(rc.resourceName, rc.resourceCount);
                }
            }

            else
            {
                // Return resources for the salvaged building
                foreach (ResourceCost rc in btsData.resourceCosts)
                {
                    ResourceManager.rm.AddResource(rc.resourceName, rc.resourceCount);
                }
            }

            // Play construction sound at source of building
            MasterAudioController.mac.PlaySfxByID(buildingToSalvage.transform.position, "playerSalvage");

            // Destroy the building
            Destroy(buildingToSalvage);
            // Set buildingToSalvage and btsData to null
            buildingToSalvage = null;
            btsData = null;
        }
    }
}
