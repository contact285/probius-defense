﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildPreview : MonoBehaviour
{
    public Color allowBuild;
    public Color notAllowBuild;

    bool spaceOccupied = false;
    bool inCoreAOI = false;
    public List<GameObject> occupyingObjects = new List<GameObject>();
    public List<GameObject> occupyingAOIs = new List<GameObject>();
    MeshRenderer[] rends = { };

    // Start is called before the first frame update
    void Start()
    {
        rends = GetComponentsInChildren<MeshRenderer>();
        UpdateRendsColor();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        
    }

    void UpdateRendsColor()
    {
        //Debug.Log(occupyingObjects.Count);
        foreach (GameObject obj in occupyingObjects)
        {
            //Debug.LogWarning(obj.name);
        }
        if (occupyingObjects.Count > 0)
        {
            spaceOccupied = true;
        }
        else
        {
            spaceOccupied = false;
        }
        if (occupyingAOIs.Count > 0)
        {
            inCoreAOI = true;
        }
        else inCoreAOI = false;

        if (spaceOccupied == true || inCoreAOI == false)
        {

            foreach(MeshRenderer rend in rends)
            {
                foreach (Material m in rend.materials)
                {
                    m.color = notAllowBuild;
                }
            }
        }
        else
        {
            foreach (MeshRenderer rend in rends)
            {
                foreach (Material m in rend.materials)
                {
                    m.color = allowBuild;
                }
            }
        }
    }

    public bool CheckCanBuild()
    {
        return (spaceOccupied == false && inCoreAOI == true);
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject.layer);
        if (other.gameObject.layer != 18) occupyingObjects.Add(other.gameObject);
        else occupyingAOIs.Add(other.gameObject);
        UpdateRendsColor();
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer != 18) occupyingObjects.Remove(other.gameObject);
        else occupyingAOIs.Remove(other.gameObject);
        UpdateRendsColor();
    }
}
