﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[System.Serializable]
public class Building 
{
    public string name;
    public GameObject preview;
    public GameObject prefab;

    public List<ResourceCost> resourceCosts;

    // These are now handled by StaticBuildingUnitController
    //public bool destructable = true;
    //public int health;

}

[CreateAssetMenu(fileName = "BuildingData", menuName = "ScriptableObjects/PlaceableBuildingsObject", order = 1)]
public class PlaceableBuildings : ScriptableObject
{
    public List<Building> buildings = new List<Building>();
    public string selectedBuilding = "";

    public List<string> GetBuildingNames()
    {
        var names = new List<string>();
        foreach (Building building in buildings)
        {
            names.Add(building.name);
        }
        return names;
    }

    public Building FindBuildingByName(string name)
    {
        return buildings.Find(building => building.name == name);
    }

    public void SelectBuilding(string name)
    {
        selectedBuilding = name;
    }

    public string GetSelectedBuilding()
    {
        return selectedBuilding;
    }

}
