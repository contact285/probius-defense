﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticBuildingUnitController : UnitController
{
    public void Start()
    {
        health = startingHP;
        //InvokeRepeating("DrainHP", 0, 1);

        // If we don't set a time to die long enough for at least 1 frame to pass,
        // turrets will try to kill non-existent objects after they die
        //if (TimeToDie < Time.deltaTime) TimeToDie = 10 * Time.deltaTime;
    }
    override protected void StateUpdate()
    {
        // Any weird state changes? Probably not.
    }

    public override void ExternalDamage(int damage)
    {
        var hitResult = TakeDamage(damage);
        if (hitResult == true)
        {
            //Debug.Log(gameObject.name + " took fatal damage!");
            currentState = state.DEATH;
            // play animations, sounds, etc. here
            Destroy(gameObject, TimeToDie);
        }
        
    }

    //void DrainHP()
    //{
    //    ExternalDamage(1);
    //}
}
