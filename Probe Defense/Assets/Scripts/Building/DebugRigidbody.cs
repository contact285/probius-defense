﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugRigidbody : MonoBehaviour {

    Rigidbody rb;
    ParticleSystem ps;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        ps = GetComponent<ParticleSystem>();
        rb.AddForceAtPosition(new Vector3(0, 0, -300), transform.position);
    }
	
	// Update is called once per frame
	void Update () {
        //Debug.Log(rb.velocity);

        //if (Input.GetKey("r"))
        //{
        //    rb.AddForceAtPosition(new Vector3(0, 0, 3), transform.position);
        //}
        //if (Input.GetKey("t"))
        //{
        //    rb.AddForceAtPosition(new Vector3(0, 0, -3), transform.position);

        //}
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Projectile") ps.Play();
    }
}
