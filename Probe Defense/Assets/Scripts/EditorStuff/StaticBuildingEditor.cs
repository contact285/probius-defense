﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/// <summary>
/// Editor script to hide unnecessary UnitController properties
/// </summary>
[CustomEditor(typeof(StaticBuildingUnitController)), CanEditMultipleObjects]
public class StaticBuildingEditor : Editor
{
    SerializedProperty
        startingHPProp,
        timeToDieProp;

    private void OnEnable()
    {
        startingHPProp = serializedObject.FindProperty("startingHP");
        timeToDieProp = serializedObject.FindProperty("TimeToDie");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(startingHPProp, new GUIContent("Starting HP"));
        EditorGUILayout.PropertyField(timeToDieProp, new GUIContent("Time to Die"));


        serializedObject.ApplyModifiedProperties();
    }
}
