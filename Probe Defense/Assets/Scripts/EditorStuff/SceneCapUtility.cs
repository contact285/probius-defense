﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;

public class SceneCapUtility : MonoBehaviour
{
    static int width = 1920;
    static int height = 1080;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Add a menu item named "Do Something with a Shortcut Key" to MyMenu in the menu bar
    // and give it a shortcut (ctrl-g on Windows, cmd-g on macOS).
    [MenuItem("CBC Utilities/Take Screenshot from Selected Camera #&c")]
    static void TakeScreenshotFromCam()
    {
        if (Selection.activeTransform != null && Selection.activeTransform.GetComponent<Camera>() != null)
        {
            Debug.Log("Taking screenshot...");
            CamCapture();
        }
        else Debug.Log("No cam selected");
    }

    static void CamCapture()
    {
        // Get cam from current selection
        Camera cam = Selection.activeTransform.GetComponent<Camera>();   

        // Create a new rendertexture of the given size/format
        RenderTexture rt = new RenderTexture(width, height, 24);
        cam.targetTexture = rt;
        // Create texture from the same rendertexture size
        Texture2D screenShot = new Texture2D(width, height, TextureFormat.RGB24, false);
        cam.Render();
        RenderTexture.active = rt;
        // Read the newly-rendered texture (from the cam) to the 2d texture
        screenShot.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        // Clean up between shots
        cam.targetTexture = null;
        RenderTexture.active = null; // added to avoid errors
        DestroyImmediate(rt);

        // Encode screenshot bytes to png-format bytes
        byte[] bytes = screenShot.EncodeToPNG();
        string filename = "Assets/Resources/levelThumbnails/" + SceneManager.GetActiveScene().name + ".png";
        // Write bytes to PNG file for this level, whether or not it exists
        System.IO.File.WriteAllBytes(filename, bytes);
        Debug.Log(string.Format("Sent screenshot to: {0}", filename));
        // Immediately refresh the asset database, so we can actually SEE the screenshot after taking it
        AssetDatabase.Refresh();
    }

}
