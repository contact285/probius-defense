﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AttackComponent)), CanEditMultipleObjects]
public class AttackComponentEditor : Editor
{
    SerializedProperty
        attackTypeProp,
        foeLayerProp,
        weaponPrefabProp,
        attackPointProp,
        weaponSpeedProp,
        orientSpeedProp,
        fireRateProp,
        ammoConsumptionProp;

    private void OnEnable()
    {
        attackTypeProp = serializedObject.FindProperty("attackType");
        foeLayerProp = serializedObject.FindProperty("foeLayer");
        weaponPrefabProp = serializedObject.FindProperty("projectilePrefab");
        attackPointProp = serializedObject.FindProperty("attackPoint");
        weaponSpeedProp = serializedObject.FindProperty("projectileSpeed");
        orientSpeedProp = serializedObject.FindProperty("projectileOrientSpeed");
        fireRateProp = serializedObject.FindProperty("fireRate");
        ammoConsumptionProp = serializedObject.FindProperty("ammoConsumption");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(attackTypeProp);
        //Activations.ActivationType type = (Activations.ActivationType)currentType_Prop.enumValueIndex;
        AttackComponent.AttackType type = (AttackComponent.AttackType)attackTypeProp.enumValueIndex;
        EditorGUILayout.PropertyField(foeLayerProp);
        EditorGUILayout.PropertyField(weaponPrefabProp, new GUIContent("Attack Weapon Prefab"));
        EditorGUILayout.PropertyField(ammoConsumptionProp, new GUIContent("Requisite Ammo"));

        switch(type)
        {
            case AttackComponent.AttackType.ELECTRIC:
                EditorGUILayout.PropertyField(attackPointProp, new GUIContent("Attack Point"));
                EditorGUILayout.PropertyField(fireRateProp, new GUIContent("Fire Rate"));
                break;
            case AttackComponent.AttackType.BULLET:
                EditorGUILayout.PropertyField(attackPointProp, new GUIContent("Barrel Tip"));
                EditorGUILayout.PropertyField(fireRateProp, new GUIContent("Fire Rate"));
                break;
            case AttackComponent.AttackType.HOMING:
                EditorGUILayout.PropertyField(attackPointProp, new GUIContent("Launch Point"));
                EditorGUILayout.PropertyField(weaponSpeedProp, new GUIContent("Homing Speed"));
                EditorGUILayout.PropertyField(orientSpeedProp, new GUIContent("Homing Orient Speed"));
                EditorGUILayout.PropertyField(fireRateProp, new GUIContent("Fire Rate"));
                break;
        }

        serializedObject.ApplyModifiedProperties();
    }
}
