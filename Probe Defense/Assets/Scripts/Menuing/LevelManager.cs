﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public static LevelManager lm { get; private set; }

    public float levelLoadStatus = 0;
    AsyncOperation loadLevel;

    public bool wait = false;
    public bool returnToLevelSelect = false;

    private void Awake()
    {
        if (lm != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            lm = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (loadLevel != null && loadLevel.progress < 0.9f)
        {
            //Debug.Log("AA");
            levelLoadStatus = loadLevel.progress;
        }
        else if (loadLevel != null && loadLevel.progress == 0.9f && wait == false)
        {
            //Debug.Log("Ready to load!");
            StartCoroutine(ReadyToLoad());
        }
        else if (loadLevel != null && loadLevel.progress == 1)
        {
            //Debug.Log("DONE");
            wait = false;
            loadLevel = null;
            levelLoadStatus = 0;
        }
    }

    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene("Main Menu");
        loadLevel = null;
    }

    public void RequestLoadLevel(string levelName)
    {
        //try
        //{

        //}
        //catch (UnityException e)
        //{
        //    Debug.LogError("Couldn't find a scene called ' " + levelName + "'.");
        //    Debug.LogError(e);
        //}
        //SceneManager.LoadScene(levelName);
        if (wait == false)
        {
            Debug.Log("Loading next level!");
            loadLevel = SceneManager.LoadSceneAsync(levelName);
            Debug.Log("disabling scene activation");
            loadLevel.allowSceneActivation = false;
        }
    }

    // Immediately load current scene
    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void RequestNextLevel()
    {
        if (wait == false)
        {
            Debug.Log(SceneManager.GetActiveScene().path);
            var currentIndex = SceneUtility.GetBuildIndexByScenePath(SceneManager.GetActiveScene().path);
            if (currentIndex + 1 < SceneManager.sceneCountInBuildSettings)
            {
                loadLevel = SceneManager.LoadSceneAsync(SceneUtility.GetScenePathByBuildIndex(currentIndex + 1));
                //Debug.Log("disabling scene activation");
                loadLevel.allowSceneActivation = false;
            }
            else
            {
                ReturnToMainMenu();
            }
        }
    }

    public float GetLevelLoadStatus()
    {
        return levelLoadStatus;
    }

    IEnumerator ReadyToLoad()
    {
        //Debug.Log("Wait!");
        wait = true;
        yield return new WaitForSeconds(1f);
        if (loadLevel != null)
        {
            //Debug.Log("Allowing scene activation");
            loadLevel.allowSceneActivation = true;
        }
    }

    public void ResetLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Quit()
    {
        Debug.Log("Quitting!");
        Application.Quit();
    }

    public void SetLevelSelectFlag()
    {
        returnToLevelSelect = true;
    }

    public bool TripLevelSelectFlag()
    {
        if (returnToLevelSelect == true)
        {
            // Indicate that we are returning to the level select for the current save, but set the flag back to false;
            returnToLevelSelect = false;
            return true;
        }
        else return false;
    }
}
