﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class MainMenuManager : MonoBehaviour
{
    public static MainMenuManager mmm { get; private set; }

    public List<GameObject> uiElements;
    public List<Transform> saveFileSlots;

    public Transform levelSelectParent;
    public GameObject levelSelectButton;

    public Button potatoButton;
    public Button HMNButton;

    public GameLevelInfo gameLevelInfo;
    public Image levelPreview;
    public TMP_Text levelName;
    public TMP_Text levelDesc;

    public Slider Master;
    public Slider Music;
    public Slider Sfx;

    // Gameobjects to select in each menu
    public GameObject firstSaveNew;
    public GameObject firstSaveStart;
    // Parent of level buttons (to help select first by default)
    public GameObject levelHolder;

    // Sfx for buttons
    public AudioSource menuSFX;
    public AudioClip forwardClip;
    public AudioClip backClip;

    // Track where settings should return to
    string lastScreen = "";
    public GameObject levelUI;
    public GameObject saveUI;

    private void Awake()
    {
        if (mmm != null)
        {
            Destroy(mmm.gameObject);
            mmm = this;
        }
        else
        {
            mmm = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (LevelManager.lm != null && LevelManager.lm.TripLevelSelectFlag() == true)
        {
            // Immediately display level select
            ToggleUIElement(uiElements[1]);
            PopulateLevelSelectButtons();
        }

        UpdateAudioSliders();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ToggleUIElement(GameObject element)
    {
        foreach(GameObject obj in uiElements)
        {
            if (obj == element) obj.SetActive(true);
            else obj.SetActive(false);
        }

        // Quick catch, since we don't *know* if a given element is settings- lazy, but cheap operation
        UpdateAudioSliders();
    }

    public void SetPreviousSettingScreen(string last)
    {
        lastScreen = last;
    }

    public void SettingsReturn()
    {
        if (lastScreen == "save")
        {
            ToggleUIElement(saveUI);
            // THIS IS DEFAULT, NEEDS FIXING
            SetStartingSelection("new");
        }
        else if (lastScreen == "level")
        {
            ToggleUIElement(levelUI);
            PopulateLevelSelectButtons();
            SetLevelButtonSelection();
        }
        else Debug.LogError("Could not find screen to return to: " + lastScreen);
    }

    public void PopulateLevelSelectButtons()
    {
        //for (int i=1; i<SceneManager.sceneCountInBuildSettings; i++)
        //{
        //    Debug.Log(SceneUtility.GetScenePathByBuildIndex(i));
        //    var newButton = Instantiate(levelSelectButton, levelSelectParent);
        //    newButton.GetComponent<LevelSelectButton>().SetLevelPath(SceneUtility.GetScenePathByBuildIndex(i));
        //    newButton.GetComponent<LevelSelectButton>().SetLevelNumber(i);
        //    // Change this to pull from level struct
        //    newButton.GetComponent<LevelSelectButton>().SetLevelName(
        //        System.IO.Path.GetFileNameWithoutExtension(
        //            SceneUtility.GetScenePathByBuildIndex(i)
        //            )
        //        );
        //    // Add level description from level struct to button
        //}

        var levelData = gameLevelInfo.GetData();
        //foreach(LevelData ld in gameLevelInfo.GetData())
        for (int i=0; i < levelData.Count; i++)
        {
            var newButton = Instantiate(levelSelectButton, levelSelectParent);
            var buttonComp = newButton.GetComponent<LevelSelectButton>();
            buttonComp.SetLevelPath(SceneUtility.GetScenePathByBuildIndex(i+1));
            buttonComp.SetLevelNumber(i+1);
            buttonComp.SetLevelName(levelData[i].levelTitle);
            buttonComp.SetLevelID(
                System.IO.Path.GetFileNameWithoutExtension(
                        SceneUtility.GetScenePathByBuildIndex(i+1)
                        )
                    );
            buttonComp.SetLevelDesc(levelData[i].levelDesc);
        }


        // Select the first level button after populating buttons
        SetLevelButtonSelection();
    }

    public void DepopulateLevelSelectButtons()
    {
        for (int i=levelSelectParent.childCount-1; i >= 0; i--)
        {
            Destroy(levelSelectParent.GetChild(i).gameObject);
        }
    }

    public void PopulateSaveFileSlots(List<SaveFile> slots)
    {
        for (int i=0; i<saveFileSlots.Count; i++)
        {
            if (slots[i].newFile == false)
            {
                var levelsCompleted = slots[i].levelsCompleted;
                var timePlayed = TimeSpan.FromSeconds(slots[i].timePlayed).ToString("hh':'mm':'ss");
                var progression = slots[i].progressionPercentage;
                Debug.Log(levelsCompleted);
                Debug.Log(timePlayed);
                Debug.Log(progression);

                // if file isn't considered empty
                // Get TMP (second child) and set texts
                // Enable start button (third child)
                // Enable the thumbnail (fifth child)
                // Disable new save button (fourth child)

                saveFileSlots[i].GetChild(1).GetComponent<TMP_Text>().text =
                    "Levels Complete: " + levelsCompleted
                    + "\n\nTime played: " + timePlayed
                    + "\n\nProgression: " + progression + "%";
                saveFileSlots[i].GetChild(1).gameObject.SetActive(true);
                saveFileSlots[i].GetChild(2).gameObject.SetActive(true);
                saveFileSlots[i].GetChild(4).gameObject.SetActive(true);
                saveFileSlots[i].GetChild(3).gameObject.SetActive(false);

                SetStartingSelection("start");

            }
            // otherwise,
            // Disable TMP and start button (2 & 3 children)
            // Disable thumbnail (fifth child)
            // Enable new save button (fourth child)
            else
            {
                saveFileSlots[i].GetChild(1).gameObject.SetActive(false);
                saveFileSlots[i].GetChild(2).gameObject.SetActive(false);
                saveFileSlots[i].GetChild(4).gameObject.SetActive(false);
                saveFileSlots[i].GetChild(3).gameObject.SetActive(true);

                SetStartingSelection("new");
            }
        }
    }

    /// <summary>
    /// Called by New Save buttons (since Level Manager may leave scene and break connections :c )
    /// </summary> 
    public void RequestFirstLevel(int index)
    {
        if (SaveManager.sm != null)
        {
            SaveManager.sm.SetCurrentSaveSlot(index);
        }
        if (LevelManager.lm != null)
        {
            LevelManager.lm.RequestNextLevel();
        }
        else Debug.LogError("Level Manager has been horribly lost to the void. Report this please! :c");
    }

    public void SetInProgressSaveFile(int index)
    {
        if (SaveManager.sm != null)
        {
            SaveManager.sm.SetCurrentSaveSlot(index);
        }
    }

    public void ReturnToSavesFromLevels()
    {
        if (SaveManager.sm != null)
        {
            SaveManager.sm.SetCurrentSaveSlot(-1);
            SaveManager.sm.LoadLevels();
        }
    }

    public void InitGraphicSettingsButtons(bool potatoNVID)
    {
        if (potatoNVID == false)
        {
            //potatoButton.interactable = false;
            //HMNButton.interactable = true;
        }   
        else
        {
            //potatoButton.interactable = true;
            //HMNButton.interactable = false;
        }
    }

    public void ToggleGraphicsSettings()
    {
        var qualityLevelName = QualitySettings.names[QualitySettings.GetQualityLevel()];
        if (qualityLevelName == "HireMeNVIDIA")
        {
            //potatoButton.interactable = false;
            //HMNButton.interactable = true;
            SettingsManager.sm.ActivateQualityLevel("Potato");
            PlayerPrefs.SetString("QualityPreset", "Potato");
        }
        else
        {
            //potatoButton.interactable = true;
            //HMNButton.interactable = false;
            SettingsManager.sm.ActivateQualityLevel("HireMeNVIDIA");
            PlayerPrefs.SetString("QualityPreset", "HireMeNVIDIA");
        }
    }

    public void HoverLevelButton(string id, string name, string desc)
    {
        Debug.Log("Hovering over level:\n" + id);
        
        // Load level preview, if one exists
        var previewSprite = Resources.Load<Sprite>("levelThumbnails/" + id);
        if (previewSprite != null)
        {
            levelPreview.sprite = previewSprite;
            if (levelPreview.gameObject.activeSelf == false) levelPreview.gameObject.SetActive(true);
            if (levelName.gameObject.activeSelf == false) levelName.gameObject.SetActive(true);
            if (levelDesc.gameObject.activeSelf == false) levelDesc.gameObject.SetActive(true);
        }
        else Debug.LogError("No preview sprite found for level \"" + id + "\". Please report this!");

        // Load level title, if one exists
        if (name != "") levelName.text = name;
        else Debug.LogError("No preview title found for level \"" + id + "\". Please report this!");


        // Load level description, if one exists
        if (desc != "") levelDesc.text = desc;
        else Debug.LogError("No preview description found for level \"" + id + "\". Please report this!");
    }

    public void UnHoverLevelButton()
    {
        levelPreview.gameObject.SetActive(false);
        levelName.gameObject.SetActive(false);
        levelDesc.gameObject.SetActive(false);
    }

    void UpdateAudioSliders()
    {
        Master.value = PlayerPrefs.GetFloat("MasterVol");
        Music.value = PlayerPrefs.GetFloat("MusicVol");
        Sfx.value = PlayerPrefs.GetFloat("SfxVol");
    }

    void SetStartingSelection(string newStart)
    {
        //EventSystem.current.sendNavigationEvents = false;
        EventSystem.current.SetSelectedGameObject(null);
        if (newStart == "new")
        {
            StartCoroutine(SelectAtEndOfFrame(firstSaveNew));
        }
        else
        {
            StartCoroutine(SelectAtEndOfFrame(firstSaveStart));
        }
    }

    void SetLevelButtonSelection()
    {
        // Clears the currently selected UI object
        EventSystem.current.SetSelectedGameObject(null);
        GameObject firstLevelButton = levelHolder.transform.GetChild(0).gameObject;
        if (firstLevelButton != null) StartCoroutine(SelectAtEndOfFrame(firstLevelButton));
    }

    public void SetSettingSelection()
    {
        EventSystem.current.SetSelectedGameObject(null);
        StartCoroutine(SelectAtEndOfFrame(potatoButton.gameObject));
    }

    IEnumerator SelectAtEndOfFrame(GameObject obj)
    {
        yield return new WaitForEndOfFrame();
        EventSystem.current.SetSelectedGameObject(obj);
        //EventSystem.current.sendNavigationEvents = true;
    }

    public void PlayForwardSound()
    {
        menuSFX.clip = forwardClip;
        menuSFX.Play();
    }

    public void PlayBackSound()
    {
        menuSFX.clip = backClip;
        menuSFX.Play();
    }

    public void Quit()
    {
        LevelManager.lm.Quit();
    }

}
