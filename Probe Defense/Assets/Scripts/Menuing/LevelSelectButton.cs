﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LevelSelectButton : MonoBehaviour
{
    public TMP_Text buttonText;

    string levelPath;
    string levelName;
    string levelID;
    string levelDesc;
    int levelNumber;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetLevelPath(string path)
    {
        levelPath = path;
    }

    public void SetLevelNumber(int number)
    {
        levelNumber = number;
        buttonText.text = levelNumber.ToString();
    }

    public void SetLevelName(string name)
    {
        levelName = name;
    }

    public void SetLevelID(string id)
    {
        levelID = id;
    }

    public void SetLevelDesc(string desc)
    {
        levelDesc = desc;
    }

    public void RequestLoad()
    {
        LevelManager.lm.RequestLoadLevel(levelPath);
    }

    public void TriggerButtonHover()
    {
        MainMenuManager.mmm.HoverLevelButton(levelID, levelName, levelDesc);
    }

    public void TriggerButtonUnhover()
    {
        MainMenuManager.mmm.UnHoverLevelButton();
    }
}
