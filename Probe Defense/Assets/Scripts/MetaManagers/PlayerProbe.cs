﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerProbe : MonoBehaviour
{
    #region Necessary Components (assign in editor)
    Animator anim;
    Rigidbody rb;
    GridPlacement gp;
    DeconstructBuildings db;
    #endregion

    #region Setting Variables
    public Transform CameraHolder;
    public GameObject reticlePrefab;
    Transform reticle;

    public float moveStrength = 1.0f;
    public float movementDrag = 0.8f;
    [SerializeField]
    Vector2 movementVector = Vector2.zero;
    public bool moving = false;

    float verticalMovement;
    public float vertMoveStrength = 1.0f;

    public float lookStrength = 10f;
    [SerializeField]
    float nextFrameRot;
    [SerializeField]
    Vector2 rotationVector = Vector2.zero;
    public bool looking = false;
    public float lookYMin;
    public float lookYMax;
    [SerializeField]
    float yRot;

    public float playerLookDistance = 3.0f;
    public float playerInteractRadius = 1.0f;

    #endregion

    #region State Variables
    bool building = false;
    bool salvaging = false;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        gp = GetComponent<GridPlacement>();
        db = GetComponent<DeconstructBuildings>();

        //reticle = Instantiate(reticlePrefab).transform;
    }

    // Update is called once per frame
    void Update()
    {
        GetPlayerLookLocation();
        if (InGameManager.im != null && InGameManager.im.GetPaused() == true && GetComponent<AudioSource>().isPlaying == true)
            GetComponent<AudioSource>().Pause();
        else if (InGameManager.im != null && InGameManager.im.GetPaused() == false && GetComponent<AudioSource>().isPlaying == false)
            GetComponent<AudioSource>().UnPause();
    }

    private void FixedUpdate()
    {
        if (!moving && movementVector != Vector2.zero)
        {
            movementVector *= movementDrag;
        }
        if (movementVector.sqrMagnitude < 0.000001f) movementVector = Vector2.zero;

        if (!looking && rotationVector != Vector2.zero) rotationVector = Vector2.zero;

        // Clarification- in testing scenes where IGM doesn't exist, this should still work
        if (InGameManager.im == null || InGameManager.im.GetPaused() == false)
            PerformMovementRotation();

        
    }

    public void OnConfirm(InputAction.CallbackContext ctx)
    {
        // Clarification- in testing scenes where IGM doesn't exist, this should still work
        if (InGameManager.im == null || InGameManager.im.GetPaused() == false)
        {
            var value = ctx.ReadValue<float>();
            if (building == true && value == 0)
            {
                gp.PlaceSelectedBuilding();
            }
            else if (salvaging == true)
            {
                db.SalvageBuilding();
            }
        }
    }

    public void OnRotate(InputAction.CallbackContext ctx)
    {
        //var value = ctx.ReadValue<float>();
        //Debug.Log(ctx.);

        // Clarification- in testing scenes where IGM doesn't exist, this should still work
        if (InGameManager.im == null || InGameManager.im.GetPaused() == false)
        {
            if (ctx.performed)
            {
                if (building == true)
                {
                    gp.RotateSelectedBuilding(-1, false);
                }
            }
        }
    }

    public void OnAltRotate(InputAction.CallbackContext ctx)
    {
        //Debug.Log(ctx.started);
        // Clarification- in testing scenes where IGM doesn't exist, this should still work
        if (InGameManager.im == null || InGameManager.im.GetPaused() == false)
        {
            if (ctx.performed)
            {
                if (building == true)
                {
                    gp.RotateSelectedBuilding(1, false);
                }
            }
        }
    }

    public void OnAltRotateKeyboard(InputAction.CallbackContext ctx)
    {
        // Clarification- in testing scenes where IGM doesn't exist, this should still work
        if (InGameManager.im == null || InGameManager.im.GetPaused() == false)
        {
            if (ctx.performed)
            {
                if (building == true)
                {
                    gp.RotateSelectedBuilding(2, false);
                }
            }
        }
    }

    public void OnMove(InputAction.CallbackContext ctx)
    {
        // Clarification- in testing scenes where IGM doesn't exist, this should still work
        if (InGameManager.im == null || InGameManager.im.GetPaused() == false)
        {
            var input = ctx.ReadValue<Vector2>();
            //Debug.Log(input);
            if (input != Vector2.zero)
            {
                movementVector = input;
                moving = true;
                anim.SetBool("Move", true);
            }
            else
            {
                moving = false;
                anim.SetBool("Move", false);
            }
        }
    }

    public void OnLook(InputAction.CallbackContext ctx)
    {
        // Clarification- in testing scenes where IGM doesn't exist, this should still work
        if (InGameManager.im == null || InGameManager.im.GetPaused() == false)
        {
            var input = ctx.ReadValue<Vector2>();
            //Debug.Log(input);
            if (input != Vector2.zero)
            {
                rotationVector = input;
                looking = true;
            }
            else looking = false;
        }
    }

    public void OnVerticalMove(InputAction.CallbackContext ctx)
    {
        // Clarification- in testing scenes where IGM doesn't exist, this should still work
        if (InGameManager.im == null || InGameManager.im.GetPaused() == false)
        {
            verticalMovement = ctx.ReadValue<float>();
        }
    }

    public void ToggleBuilding(InputAction.CallbackContext ctx)
    {
        // Clarification- in testing scenes where IGM doesn't exist, this should still work
        if (InGameManager.im == null || InGameManager.im.GetPaused() == false)
        {
            building = !building;
            if (AOIManager.aoim != null) AOIManager.aoim.ToggleAOIVisibility(building);
            if (building == true) salvaging = false;
        }
    }

    public void ToggleSalvaging(InputAction.CallbackContext ctx)
    {
        // Clarification- in testing scenes where IGM doesn't exist, this should still work
        if (InGameManager.im == null || InGameManager.im.GetPaused() == false)
        {
            salvaging = !salvaging;
            if (salvaging == true) building = false;
        }
    }

    void PerformMovementRotation()
    {
        // Rotate 
        Vector3 deltaRotEuler = new Vector3(-rotationVector.y, rotationVector.x, 0) * Time.fixedDeltaTime * lookStrength;
        var result = rb.rotation.eulerAngles + deltaRotEuler;
        result.z = 0;

        if (result.x > 180) result.x -= 360;

        if (result.x > lookYMax) result.x = lookYMax;
        else if (result.x < lookYMin) result.x = lookYMin;
        //if (result.x > 180) result.x = 0 - result.x;
       
        //Debug.Log(result);
        rb.MoveRotation(Quaternion.Euler(result));

        // Move
        Vector3 fMove = transform.forward * Time.fixedDeltaTime * moveStrength * movementVector.y;
        Vector3 hMove = transform.right * Time.fixedDeltaTime * moveStrength * movementVector.x;
        Vector3 vMove = transform.up * Time.fixedDeltaTime * vertMoveStrength * verticalMovement;
        //Debug.Log(fMove + " " + hMove);
        rb.velocity = fMove + hMove + vMove;
        //Debug.Log(rb.velocity);

        // Set flight animation
        anim.SetFloat("Fly Input X", movementVector.x, 0.05f, Time.fixedDeltaTime);
        anim.SetFloat("Fly Input Y", movementVector.y, 0.05f, Time.fixedDeltaTime);
    }

    void GetPlayerLookLocation()
    {
        // Update building UI canvas
        if (building == true)
        {
            RaycastHit hit;
            int hitMask = (1 << 8) | (1 << 9);
            //hitMask = ~hitMask;
            //Physics.Raycast(transform.position, transform.forward, out hit, playerLookDistance);
            Physics.SphereCast(CameraHolder.position, playerInteractRadius, CameraHolder.forward, out hit, playerLookDistance, hitMask);
            if (hit.collider != null)
            {
                //Debug.Log(hit.point);
                gp.SetBuildCanvasActive(true);
                //Debug.Log(hit.transform.tag);
                if (hit.transform.tag == "Node" && gp.GetBuildingTag() == "Production")
                {
                    //Debug.Log("Looking at resource node to place production, snap to center");
                    gp.UpdateBuildUITransform(new Vector3(hit.transform.position.x, hit.point.y, hit.transform.position.z), transform, Vector3.Distance(CameraHolder.position, hit.point));
                }
                else if (hit.transform.tag == "TurretPad" && gp.GetBuildingTag() == "Turret")
                {
                    //Debug.Log("Looking at turret pad to place turret, snap to center");
                    gp.UpdateBuildUITransform(new Vector3(hit.transform.position.x, hit.point.y, hit.transform.position.z), transform, Vector3.Distance(CameraHolder.position, hit.point));
                }
                else gp.UpdateBuildUITransform(hit.point, transform, Vector3.Distance(CameraHolder.position, hit.point));
            }
            else
            {
                gp.UpdateBuildUITransform(CameraHolder.position, transform, 2);
                gp.SetBuildCanvasActive(false);
            }
        }
        else if (building == false)
        {
            gp.UpdateBuildUITransform(CameraHolder.position, transform, 2);
            gp.SetBuildCanvasActive(false);
        }

        // Update salvage UI canvas
        if (salvaging == true)
        {
            RaycastHit hit;
            int hitMask = 1 << 17;
            //hitMask = ~hitMask;
            //Physics.Raycast(transform.position, transform.forward, out hit, playerLookDistance);
            Physics.Raycast(CameraHolder.position, CameraHolder.forward, out hit, playerLookDistance, hitMask);
            if (hit.collider != null)
            {
                Debug.Log(hit.transform.gameObject.name);
                db.SetSalvageCanvasActive(true);
                db.UpdateSalvageUITransform(hit.point, transform, Vector3.Distance(CameraHolder.position, hit.point));
                // Building layer is 
                if (hit.transform.gameObject.layer == 17)
                {
                    db.SetSalvagedBuilding(hit.transform.gameObject);
                }
            }
            else
            {
                db.UpdateSalvageUITransform(CameraHolder.position, transform, 2);
                db.SetSalvageCanvasActive(false);
                db.ResetBuildingToSalvage();
            }
        }
        else if (building == false)
        {
            db.UpdateSalvageUITransform(CameraHolder.position, transform, 2);
            db.SetSalvageCanvasActive(false);
        }

        // Update player look reticle
        //RaycastHit reticleHit;
        //Physics.Raycast(CameraHolder.position, CameraHolder.forward, out reticleHit, playerLookDistance);
        //if (reticleHit.collider != null)
        //{
        //    if (reticle.gameObject.activeSelf == false) reticle.gameObject.SetActive(true);
        //    reticle.position = reticleHit.point;
        //    reticle.LookAt(CameraHolder);
        //}
        //else
        //{
        //    if (reticle.gameObject.activeSelf == true) reticle.gameObject.SetActive(false);
        //}
    }

}
