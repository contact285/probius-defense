﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class InGameManager : MonoBehaviour
{
    public static InGameManager im { get; private set; }
    [SerializeField] bool paused;

    public int friendlyCoreDeathsToLose = 1;
    int friendlyCoreDeaths = 0;
    public int enemyCoreDeathsToWin = 1;
    int enemyCoreDeaths = 0;

    //public GameObject pauseMenu;
    //public GameObject levelSelectConfirm;
    //public GameObject settingsUI;
    //public GameObject quitToDesktopConfirm;

    public List<GameObject> pauseUIElements = new List<GameObject>();
    public GameObject rootPauseUI;

    public Button potatoButton;
    public Button HMNButton;

    public Slider MasterVol;
    public Slider MusicVol;
    public Slider SfxVol;

    private void Awake()
    {
        if (im != null) Destroy(im.gameObject);
        im = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        paused = false;
        if (SaveManager.sm != null)
        {
            SaveManager.sm.SetLevelStartTime();
        }
        else Debug.LogError("In-game Manager discovered that the SaveManager was eaten by grues. Please report this!");

        //if (PlayerPrefs.GetString("QualityPreset") == "Potato") InitGraphicSettingsButtons(false);
        //else InitGraphicSettingsButtons(true);
        //Debug.Log("starting scene");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EscapeTogglePause(InputAction.CallbackContext ctx)
    {
        if (ctx.ReadValue<float>() == 0)
        {
            if (paused)
            {
                paused = false;
                Time.timeScale = 1;
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
                rootPauseUI.SetActive(false);
            }
            else
            {
                paused = true;
                Time.timeScale = 0;
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                rootPauseUI.SetActive(true);
                UpdateAudioSliders();
            }
            // First element should be the root buttons holder
            DisplayPauseMenuElement(pauseUIElements[0]);
        }
    }

    // Same as above, but called via UI button rather than playerInput
    public void ReturnToGame()
    {
        if (paused)
        {
            paused = false;
            Time.timeScale = 1;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            rootPauseUI.SetActive(false);
        }
        else
        {
            paused = true;
            Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            rootPauseUI.SetActive(true);
        }
    }

    public void RestartLevel()
    {
        paused = false;
        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        //Debug.Log("Restarting");
        LevelManager.lm.RestartLevel();
        // SaveManager isn't called because we don't save success progress until completion
    }

    public void GoToLevelSelect(bool success)
    {
        Debug.Log("Level completed, saving and going to level select");
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        if (SaveManager.sm != null)
        {
            SaveManager.sm.LevelEnd(success);
            SaveManager.sm.SaveLevel();
        }
        if (LevelManager.lm != null)
        {
            Time.timeScale = 1;
            // Change to a function that returns to the level select screen (without changing saves)
            LevelManager.lm.SetLevelSelectFlag();
            LevelManager.lm.ReturnToMainMenu();
        }
        
    }

    public void GoToStart()
    {
        Debug.Log("Returning to main menu (loss?) without saving");
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        if (SaveManager.sm != null)
        {
            SaveManager.sm.LevelEnd(false);
            SaveManager.sm.SetCurrentSaveSlot(-1);
        }
        if (LevelManager.lm != null)
        {
            Time.timeScale = 1;
            LevelManager.lm.ReturnToMainMenu();
        }
        
    }

    public void QuitToDesktop()
    {
        Time.timeScale = 1;
        Debug.Log("Quitting to desktop.");
        Application.Quit();
    }

    public void DisplayPauseMenuElement(GameObject element)
    {
        foreach(GameObject obj in pauseUIElements)
        {
            if (element == obj) obj.SetActive(true);
            else obj.SetActive(false);
        }
    }

    public void SelectUIElement(GameObject obj)
    {
        EventSystem.current.SetSelectedGameObject(null);
        StartCoroutine(SelectAtEndOfFrame(obj));
    }

    IEnumerator SelectAtEndOfFrame(GameObject obj)
    {
        yield return new WaitForEndOfFrame();
        EventSystem.current.SetSelectedGameObject(obj);
        //EventSystem.current.sendNavigationEvents = true;
    }

    public void InitGraphicSettingsButtons(bool potatoNVID)
    {
        if (potatoNVID == false)
        {
            potatoButton.interactable = false;
            HMNButton.interactable = true;
        }
        else
        {
            potatoButton.interactable = true;
            HMNButton.interactable = false;
        }
    }

    public void ToggleGraphicsSettings()
    {
        var qualityLevelName = QualitySettings.names[QualitySettings.GetQualityLevel()];
        if (qualityLevelName == "HireMeNVIDIA")
        {
            //potatoButton.interactable = false;
            //HMNButton.interactable = true;
            SettingsManager.sm.ActivateQualityLevel("Potato");
            PlayerPrefs.SetString("QualityPreset", "Potato");
        }
        else
        {
            //potatoButton.interactable = true;
            //HMNButton.interactable = false;
            SettingsManager.sm.ActivateQualityLevel("HireMeNVIDIA");
            PlayerPrefs.SetString("QualityPreset", "HireMeNVIDIA");
        }
    }

    void UpdateAudioSliders()
    {
        MasterVol.value = PlayerPrefs.GetFloat("MasterVol");
        MusicVol.value = PlayerPrefs.GetFloat("MusicVol");
        SfxVol.value = PlayerPrefs.GetFloat("SfxVol");
    }

    public void DebugWinLevel(InputAction.CallbackContext ctx)
    {
        if (ctx.ReadValue<float>() == 0)
        {
            GoToLevelSelect(true);
        }
    }

    public void DebugLoseLevel(InputAction.CallbackContext ctx)
    {
        if (ctx.ReadValue<float>() == 0)
        {
            GoToLevelSelect(false);
        }
    }

    public bool GetPaused()
    {
        return paused;
    }

    /// <summary>
    /// Called externally when a core indicates it has been destroyed. Necessary for ending levels 
    /// when either the player or enemy wins.
    /// </summary>
    public void CoreDeathTrigger(string coreType)
    {
        Debug.Log("A " + coreType + " has been destroyed.");
        if (coreType == "Friendly Core")
        {
            friendlyCoreDeaths++;
            if (friendlyCoreDeaths >= friendlyCoreDeathsToLose)
            {
                Debug.Log("Player loses!");
                Time.timeScale = 0;
                GoToLevelSelect(false);
            }
        }
        else if (coreType == "Enemy Core")
        {
            enemyCoreDeaths++;
            if (enemyCoreDeaths >= enemyCoreDeathsToWin)
            {
                Debug.Log("Player wins!");
                Time.timeScale = 0;
                GoToLevelSelect(true);
            }
        }
    }
}
