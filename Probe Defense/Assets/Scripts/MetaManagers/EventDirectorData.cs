﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EventData
{
    public enum EventType
    {
        SPAWNER_ACTIVATION,
        PRODUCTION_ACTIVATION,
        PROCESSING_ACTIVATION,
    }
    public string eventName;
    public EventType type = EventType.SPAWNER_ACTIVATION;
    public int secondsToEvent = 0;
    public string eventCode;

    // Use in-game EventDirector to control lists of things to activate
    //public List<GameObject> spawners = new List<GameObject>();
    //public List<GameObject> producers = new List<GameObject>();
    //public List<GameObject> processors = new List<GameObject>();
}

[CreateAssetMenu(fileName = "EventData", menuName = "ScriptableObjects/EventDirectorData", order = 2)]
public class EventDirectorData : ScriptableObject
{
    public List<EventData> data = new List<EventData>();
    //public EventData e = new EventData();
}
