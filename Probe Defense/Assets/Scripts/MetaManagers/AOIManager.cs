﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Simple manager class that controls whether Area-of-Influence spheres are visible.
/// Accessed primarily by GridPlacement/DeconstructBuildings (when placing/salvaging hubs) and PlayerProbe (when
/// toggling build mode)
/// </summary>
public class AOIManager : MonoBehaviour
{
    public static AOIManager aoim { get; private set; }

    // Serialized so we can add Core's AOI to it in editor
    [SerializeField] List<GameObject> AOIObjects = new List<GameObject>();

    private void Awake()
    {
        if (aoim != null) Destroy(aoim.gameObject);
        aoim = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ToggleAOIVisibility(bool onOff)
    {
        foreach(GameObject aoi in AOIObjects)
        {
            aoi.SetActive(onOff);
        }
    }

    public void AddAOI(GameObject aoi)
    {
        if (!AOIObjects.Contains(aoi)) AOIObjects.Add(aoi);
        else Debug.LogWarning("Attempted to add duplicate AOI object " + aoi.name + " in " + aoi.transform.parent.name);
    }

    public void RemoveAOI(GameObject aoi)
    {
        if (AOIObjects.Contains(aoi)) AOIObjects.Remove(aoi);
        else Debug.LogWarning("Attempted to remove non-added AOI object " + aoi.name + " in " + aoi.transform.parent.name);
    }
}
