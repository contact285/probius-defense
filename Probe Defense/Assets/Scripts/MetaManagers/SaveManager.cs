﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

[Serializable]
public class SaveFile
{
    public bool newFile = true;
    // Using only an int, assume levels are currently sequential
    public int levelsCompleted = 0;
    public float timePlayed = 0;
    public int progressionPercentage = 0;

    public void AddLevelCompleted()
    {
        levelsCompleted++;
        SetProgressionPercentage();
    }

    public void AddTimePlayed(float timeToAdd)
    {
        timePlayed += timeToAdd;
    }

    public void SetProgressionPercentage()
    {
        progressionPercentage = (int)( 100 * (levelsCompleted * 1.0f) / (SceneManager.sceneCountInBuildSettings - 1));
    }

    public void FlagFileAsInProgress()
    {
        newFile = false;
    }
}

public class SaveManager : MonoBehaviour
{
    public static SaveManager sm { get; private set; }

    string saveFilePath;

    float levelStartTime;
    int currentSaveSlot;
    SaveFile currentSave;

    List<SaveFile> saveSlots = new List<SaveFile>();

    private void Awake()
    {
        if (sm != null)
        {
            Destroy(this);
        }
        else
        {
            sm = this;
            DontDestroyOnLoad(this.gameObject);
        }
        
    }

    // Start is called before the first frame update
    void Start()
    {
        Debug.LogWarning("Setting file path for first load");
        saveFilePath = Application.persistentDataPath + "\\saves";

        if (!Directory.Exists(saveFilePath))
        {
            Directory.CreateDirectory(saveFilePath);
            for (int i = 1; i <= 3; i++)
            {
                File.Create(saveFilePath + "\\save0" + i + ".sav").Dispose();
                WriteSaveToFile(new SaveFile(), saveFilePath + "\\save0" + i + ".sav", true);
            }
            File.Create(saveFilePath + "\\PleaseDoNotReportBugsForEditedOrManuallyDeletedSaves.txt").Dispose();
        }
        else
        {
            for (int i = 1; i <= 3; i++)
            {
                if (!File.Exists(saveFilePath + "\\save0" + i + ".sav"))
                {
                    Debug.LogWarning("Save " + i + " missing or renamed, re-generating.");
                    File.Create(saveFilePath + "\\save0" + i + ".sav").Dispose();
                    WriteSaveToFile(new SaveFile(), saveFilePath + "\\save0" + i + ".sav", true);

                }
            }
        }

        LoadLevels();
        // Start without a current save
        SetCurrentSaveSlot(-1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetLevelStartTime()
    {
        // Not using unscaled time because we don't want to count time spent paused
        
        levelStartTime = Time.time;
        Debug.Log("The level began at: " + levelStartTime);
    }

    public void LevelEnd(bool success)
    {
        if (success == true)
        {
            Debug.Log("The level was won!");
            currentSave.AddLevelCompleted();
        }
        else Debug.Log("The level was lost!");

        // Add total time played, regardless of success
        Debug.Log("The level has ended at " + Time.time);
        var totalLevelTime = Time.time - levelStartTime;
        Debug.Log("Adding " + totalLevelTime + " ms to the current save file total.");
        currentSave.AddTimePlayed(totalLevelTime);
        levelStartTime = -1;
    }

    /// <summary>
    /// Called externally when navigating to the Save Select menu.
    /// Loads each save slot file and asks the MMM to populate the UI with them.
    /// </summary>
    public void LoadLevels()
    {
        // Check each slot to see if its save file is empty or not
        // If not, activate the appropriate UI in MMM and populate it with the save's data
        // Otherwise, activate the "New Save" UI in MMM

        Debug.LogWarning("Setting file path after first load");
        saveFilePath = Application.persistentDataPath + "\\saves";

        try
        {
            Debug.LogWarning("Loading saves at " + saveFilePath);
            List<SaveFile> slots = new List<SaveFile>();
            for (int i=1; i<=3; i++)
            {
                SaveFile sf = ReadSaveFromFile(saveFilePath + "\\save0" + i + ".sav");
                slots.Add(sf);
            }
            saveSlots = slots;
            MainMenuManager.mmm.PopulateSaveFileSlots(saveSlots);
        }
        catch (Exception e)
        {
            Debug.LogError("Error trying to load levels: " + e);
        }
    }

    public void SaveLevel()
    {
        if (currentSaveSlot != -1)
        {
            // Update the corresponding save file with the new SaveFile data
            Debug.LogWarning("Setting file path for level end");
            saveFilePath = Application.persistentDataPath + "\\saves";

            //currentSave = ReadSaveFromFile(saveFilePath + "\\save0" + currentSaveSlot + ".sav");

            WriteSaveToFile(currentSave, saveFilePath + "\\save0" + (currentSaveSlot+1) + ".sav", false);
        }
        else Debug.LogError("Attempted to save without a valid save slot selected. CurrentSaveSlot: " + (currentSaveSlot+1));
    }

    SaveFile ReadSaveFromFile(string path)
    {
        FileStream rstream = File.Open(path, FileMode.Open);
        BinaryFormatter formatter = new BinaryFormatter();
        SaveFile readFile = (SaveFile)formatter.Deserialize(rstream);

        rstream.Close();

        return readFile;
    }

    void WriteSaveToFile(SaveFile sf, string path, bool newFile)
    {
        FileStream wstream = File.Open(path, FileMode.Create);
        if (newFile == false) sf.FlagFileAsInProgress();
        //if (!path.Contains("1")) sf.FlagFileAsInProgress();
        BinaryFormatter formatter = new BinaryFormatter();

        formatter.Serialize(wstream, sf);

        wstream.Close();
    }

    public void SetCurrentSaveSlot(int saveSlotIndex)
    {
        Debug.Log("Selecting save slot " + saveSlotIndex);
        // Should work whether or not the save is "empty"
        if (saveSlotIndex >= 0)
        {
            currentSaveSlot = saveSlotIndex;
            currentSave = saveSlots[saveSlotIndex];
            //currentSave = ReadSaveFromFile(saveFilePath + "\\save0" + currentSaveSlot + ".sav");
        }
        else
        {
            // Useful for enforcing slot loading/saving logic
            currentSaveSlot = -1;
            currentSave = null;
        }
    }

    public int GetSaveLevelCompletion(int saveSlotIndex)
    {
        return saveSlots[saveSlotIndex].levelsCompleted;
    }

    public float GetSavePlaytime(int saveSlotIndex)
    {
        return saveSlots[saveSlotIndex].timePlayed;
    }

    public float GetSaveProgression(int saveSlotIndex)
    {
        return saveSlots[saveSlotIndex].progressionPercentage;
    }

}
