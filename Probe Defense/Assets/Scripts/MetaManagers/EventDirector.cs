﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LevelEvent
{
    public string eventName;
    public string eventCode;
    public EventData.EventType eventType;
    public List<GameObject> eventObjs;
    [SerializeField] int timeElapsed = 0;

    // # of seconds to wait to trigger event (timeElapsed >= timeToFireEvent)
    public int timeToFireEvent = 0;

    public bool MatchEventCode(string code)
    {
        return eventCode == code;
    }

    public void Tick()
    {
        timeElapsed++;
    }

    public bool EventReady()
    {
        return timeElapsed >= timeToFireEvent;
    }

    public void ActivateEventObjs()
    {
        switch(eventType)
        {
            case EventData.EventType.SPAWNER_ACTIVATION:
                foreach(GameObject spawner in eventObjs)
                {
                    var unitSpawner = spawner.GetComponent<UnitSpawner>();
                    if (unitSpawner != null) unitSpawner.enabled = true;
                    else Debug.LogError("Could not find UnitSpawner component for " + spawner.name + " when executing " + eventType + " event.");
                }
                break;
            case EventData.EventType.PRODUCTION_ACTIVATION:
                foreach (GameObject producer in eventObjs)
                {
                    var resourceProducer = producer.GetComponent<ProductionMachine>();
                    if (resourceProducer != null) resourceProducer.enabled = true;
                    else Debug.LogError("Could not find ProductionMachine component for " + producer.name + " when executing " + eventType + " event.");
                }
                break;
            case EventData.EventType.PROCESSING_ACTIVATION:
                foreach (GameObject processor in eventObjs)
                {
                    var resourceProcessor = processor.GetComponent<ProcessingMachine>();
                    if (resourceProcessor != null) resourceProcessor.enabled = true;
                    else Debug.LogError("Could not find ProcessingMachine component for " + processor.name + " when executing " + eventType + " event.");
                }
                break;
            
            default:
                Debug.LogError("Cannot activate event objs for unknown type " + eventType);
                break;
        }
    }
}

public class EventDirector : MonoBehaviour
{
    public EventDirectorData levelEventData;

    public List<LevelEvent> eventStructs = new List<LevelEvent>();

    // Start is called before the first frame update
    void Start()
    {
        // Iterate over all eventData in levelEventData
        // For each eventData, find a LevelEvent whose code matches the eventData
        //      (do not include the eventData if no LevelEvent is found that matches)
        //    Once found, flag the event to count down until it should fire, using eventData "secondsToEvent"
        //    Each LevelEvent tracks how long (in seconds) it should wait to fire, and updates every second
        
        foreach(EventData ed in levelEventData.data)
        {
            LevelEvent LE = null;
            foreach(LevelEvent levelEvent in eventStructs)
            {
                if (levelEvent.MatchEventCode(ed.eventCode) == true)
                {
                    if (LE != null) Debug.LogWarning("Duplicate level events with code " + ed.eventCode + " found, this is a bug!");
                    else LE = levelEvent;
                }
            }
            if (LE == null) Debug.LogError("No level event with code " + ed.eventCode + " was found. This is a bug!");
            else
            {
                Debug.Log("Found event with code " + ed.eventCode + ". It requires " + ed.secondsToEvent + " seconds to elapse before firing.");
                BeginEventCountdown(LE, ed.secondsToEvent, ed.type);
            }
        }

        StartCoroutine(TimedEventKill(10));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BeginEventCountdown(LevelEvent le, int eventTime, EventData.EventType type)
    {
        le.timeToFireEvent = eventTime;
        le.eventType = type;
        StartCoroutine(EventCountdown(le));
    }

    IEnumerator EventCountdown(LevelEvent le)
    {
        yield return new WaitForSeconds(1);
        le.Tick();
        if (le.EventReady())
        {
            Debug.Log("Firing event " + le.eventName + "!");
            le.ActivateEventObjs();
        }
        else StartCoroutine(EventCountdown(le));
    }

    IEnumerator TimedEventKill(int seconds)
    {
        yield return new WaitForSeconds(seconds);
        KillEvents();
    }

    public void KillEvents()
    {
        // Stop all event coroutines to avoid weird bugs (esp. when changing scenes or restarting)
        Debug.Log("Killing event coroutines");
        StopAllCoroutines();
    }
}
