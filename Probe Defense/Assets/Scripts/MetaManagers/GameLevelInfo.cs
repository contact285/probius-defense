﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class LevelData
{
    //public string levelScene;
    public string levelTitle;
    public string levelDesc;

}

[CreateAssetMenu(fileName = "GameLevelData", menuName = "ScriptableObjects/GameLevelData", order = 5)]
public class GameLevelInfo : ScriptableObject
{
    public List<LevelData> data = new List<LevelData>();

    public List<LevelData> GetData()
    {
        return data;
    }

    public LevelData GetLevelByTitle(string title)
    {
        LevelData returnData = null;
         foreach(LevelData ld in data)
        {
            if (ld.levelTitle == title) returnData = ld;
        }

        return returnData;
    }
}
