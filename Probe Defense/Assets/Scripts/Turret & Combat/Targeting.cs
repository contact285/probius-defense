﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Targeting : MonoBehaviour {

    public GameObject target;
    public GameObject projectile;
    public Transform barrelTip;
    public float projectileSpeed; // Sp
    public Vector3 targetVelocity;
    public float timeToImpact;

    bool firing = false;

    Vector3 currentAngle;

	// Use this for initialization
	void Start () {
        //Debug.Log(Physics.gravity.magnitude);
        currentAngle = transform.eulerAngles;
	}
	
	// Update is called once per frame
	void Update () {
        //Debug.DrawLine(transform.position, transform.forward * 2, Color.cyan);
        if (target != null)
        {
            var move = MoveTurret();
            if (move != Vector3.zero)
            {
                //transform.LookAt(move);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(move), Time.deltaTime * 50f);
                //Debug.Log(Quaternion.Angle(transform.rotation, Quaternion.LookRotation(move)));
                /*if (Input.GetKeyDown("f") && Quaternion.Angle(transform.rotation, Quaternion.LookRotation(move)) < 1f)
                {
                    GameObject bullet = GameObject.Instantiate(projectile, barrelTip.position, Quaternion.Euler(0f, 0f, 0f));
                    bullet.GetComponent<Rigidbody>().velocity = transform.forward * projectileSpeed;

                }*/
                if (firing == false && Quaternion.Angle(transform.rotation, Quaternion.LookRotation(move)) < 1f)
                {
                    InvokeRepeating("Fire", 0f, 0.25f);
                    firing = true;
                }

            }
            else
            {
                if (Input.GetKeyDown("f")) Debug.Log("Can't hit target!");
                firing = false;
                CancelInvoke("Fire");
            }
        }
        else
        {
            currentAngle = new Vector3(
                Mathf.LerpAngle(currentAngle.x, 0f, Time.deltaTime),
                Mathf.LerpAngle(currentAngle.y, -90f, Time.deltaTime),
                Mathf.LerpAngle(currentAngle.z, 0f, Time.deltaTime));

            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(currentAngle), Time.deltaTime * 5);
            firing = false;
            CancelInvoke("Fire");
        }
            //Debug.DrawLine(transform.position, target.transform.position, Color.red);

        }

    void Fire()
    {
        GameObject bullet = GameObject.Instantiate(projectile, barrelTip.position, Quaternion.Euler(0f, 0f, 0f));
        bullet.GetComponent<Rigidbody>().velocity = transform.forward * projectileSpeed;

    }

    Vector3 MoveTurret()
    {
        // Vector from the barrel tip to the target's current position
        var toTarget = target.transform.position - barrelTip.position;
        targetVelocity = target.GetComponent<Rigidbody>().velocity;

        // Difference in the squares of the magnitudes of the target and projectile's velocities
        float a = Vector3.Dot(targetVelocity, targetVelocity) - (projectileSpeed * projectileSpeed);
        // Dot product of target velocity and distance vector
        float b = 2 * Vector3.Dot(targetVelocity, toTarget);
        // Dot product of distance vector
        float c = Vector3.Dot(toTarget, toTarget);

        float p = -b / (2 * a);
        float q = Mathf.Sqrt((b * b) - 4 * a * c) / (2 * a);

        float t1 = p + q;
        float t2 = p - q;
        //Debug.Log(t1 + " " + t2);

        float t;
        if (t1 < t2 && t1 > 0)
        {
            t = t1;
        }
        else
        {
            t = t2;
        }
        //Debug.Log(t);
        timeToImpact = t;

        if (t > 0)
        {
            var aimSpot = target.transform.position + targetVelocity * t;
            var bulletPath = aimSpot + barrelTip.position;
            float timeToImpact = bulletPath.magnitude / projectileSpeed;
            
            //return aimSpot;
            float angle;
            if (CalcTrajectory(out angle) == true)
            {
                float trajectoryHeight = Mathf.Tan(angle * Mathf.Deg2Rad) * Vector3.Distance(barrelTip.position, target.transform.position);
                //Debug.Log(trajectoryHeight);
                aimSpot.y -= trajectoryHeight;
            }
            Debug.DrawLine(barrelTip.position, aimSpot, Color.cyan);
            //Debug.DrawLine(barrelTip.position, bulletPath, Color.green);
            return aimSpot;
        }
        //else Debug.Log("Can't hit the target!");
        return Vector3.zero;
    }

    bool CalcTrajectory(out float angle)
    {
        angle = 0.5f * (Mathf.Asin(
            (Physics.gravity.y * Vector3.Distance(barrelTip.position, target.transform.position)) 
            / (projectileSpeed * projectileSpeed)) * Mathf.Rad2Deg);
        if (float.IsNaN(angle))
        {
            angle = 0f;
            return false;
        }
        return true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Target") target = other.gameObject;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Target") if (target == other.gameObject) target = null;
    }
}
