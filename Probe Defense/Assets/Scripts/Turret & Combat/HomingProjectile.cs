﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class HomingProjectile : MonoBehaviour
{
    float travelSpeed = 0;
    float orientSpeed = 0;

    public GameObject currentTarget = null;
    AttackComponent parent;
    Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        // If a target has been assigned, orient towards that target
        if (currentTarget != null)
        {
            // turn towards target
            transform.rotation = Quaternion.RotateTowards(
                transform.rotation, Quaternion.LookRotation(
                    currentTarget.GetComponent<Collider>().bounds.center - transform.position),
                    Time.deltaTime * orientSpeed);
            Debug.DrawLine(currentTarget.GetComponent<Collider>().bounds.center, transform.position, Color.magenta);
            rb.velocity = transform.forward * travelSpeed;
        }

        // move forward
        
    }

    public void SetTravelSpeed(float speed)
    {
        travelSpeed = speed;
    }

    public void SetOrientSpeed(float speed)
    {
        orientSpeed = speed;
    }

    public void AssignParent(AttackComponent ac)
    {
        parent = ac;
    }

    // TODO: fix issue where homers "drop dead" when chasing a target
    //  outside the turret's range
    public void AssignNewTarget(GameObject newTarget)
    {
        currentTarget = newTarget;
        Debug.Log("Target assigned!");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag != "Turret")
        {
            parent.RemoveHomer(this.gameObject);
            Destroy(this.gameObject);
        }
    }
}
