﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Flexible component for customizing a unit's attacks. Required by units that are able to attack.
/// Useful for when attacking units change hands from AI to player control or vice versa.
/// </summary>
public class AttackComponent : MonoBehaviour
{
    public enum AttackType
    {
        BULLET,     
        HOMING,         // Can use arbitrary speeds, depending on what feels appropriate
        ELECTRIC,
    }

    public List<string> dmgTags;
    public int foeLayer;

    [SerializeField] GameObject projectilePrefab;
    [SerializeField] AttackType attackType;
    [SerializeField] Transform attackPoint;
    [SerializeField] float projectileSpeed = 0; // if 0, projectile is hitscan (e.g. lasers)
    [SerializeField] float projectileOrientSpeed = 0; // Used for homing projectiles
    [SerializeField] float fireRate = 1f;
    public List<ResourceCost> ammoConsumption;

    // Used to keep track of all existing homing projectiles,
    //  in the case that a new target needs to be assigned
    
    List<GameObject> homingProjectiles = new List<GameObject>();

    GameObject currentTarget = null;

    GameObject bolt;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (attackType == AttackType.HOMING) UpdateHomers();
        else if (attackType == AttackType.ELECTRIC) UpdateBolt();
    }

    public void Attack(GameObject target)
    {
        //Debug.Log("Attacking!");
        if (ammoConsumption.Count == 0 || (ResourceManager.rm != null && ResourceManager.rm.CheckResourceCost(ammoConsumption)))
        {
            currentTarget = target;

            if (attackType == AttackType.BULLET)
            {
                //Debug.Log("Making bullet");
                var newBullet = Instantiate(projectilePrefab, attackPoint.position, Quaternion.Euler(0f, 0f, 0f));

                // Line-of-sight determination is now handled by the attacking unit/turret, not the attack itself
                StartCoroutine(bulletMove(Time.deltaTime * 2, newBullet, target.transform.position));
            }

            // Spawn and orient the projectile, then let it do its own thing
            else if (attackType == AttackType.HOMING)
            {
                var newHomer = Instantiate(projectilePrefab, attackPoint.position, attackPoint.rotation);
                //newHomer.transform.rotation = Quaternion.Euler(Vector3.zero);
                homingProjectiles.Add(newHomer);
                HomingProjectile homer = newHomer.GetComponent<HomingProjectile>();
                homer.SetOrientSpeed(projectileOrientSpeed);
                homer.SetTravelSpeed(projectileSpeed);
                homer.AssignNewTarget(target);
                homer.AssignParent(this);
            }

            else if (attackType == AttackType.ELECTRIC)
            {
                Debug.Log("Attacking");
                if (bolt == null)
                {
                    // Spawn in electric effect prefab
                    bolt = Instantiate(projectilePrefab, attackPoint.position, attackPoint.rotation);
                    // Set start at barrel point
                    bolt.transform.GetChild(0).transform.position = attackPoint.position;
                    // Set end at target center
                    bolt.transform.GetChild(1).transform.position = currentTarget.GetComponent<Collider>().bounds.center;
                }
                // Temporary value - use specified attack damage later
                currentTarget.GetComponent<UnitController>().ExternalDamage(1);
            }

            if (ResourceManager.rm != null) ResourceManager.rm.ConsumeFromResourceCosts(ammoConsumption);
        }
    }

    public void Attack(GameObject target, Vector3 position)
    {
        if (ammoConsumption.Count == 0 || (ResourceManager.rm != null && ResourceManager.rm.CheckResourceCost(ammoConsumption)))
        {
            // Spawn and move the projectile, but the actual hit comes from a raycast rather than the projectile itself
            // (may need to account for visual issues)
            if (attackType == AttackType.BULLET)
            {
                //Debug.Log("Making bullet");
                var newBullet = Instantiate(projectilePrefab, attackPoint.position, Quaternion.Euler(0f, 0f, 0f));

                // Line-of-sight determination is now handled by the attacking unit/turret, not the attack itself
                StartCoroutine(bulletMove(Time.deltaTime * 2, newBullet, position));
            }
        }
    }

    IEnumerator bulletMove(float tpTime, GameObject newBullet, Vector3 newPosition)
    {
        //Debug.Log("Moving bullet");
        yield return new WaitForSeconds(tpTime);

        // Did anything intercept the bullet somehow?
        if (newBullet != null)
        {
            newBullet.transform.position = newPosition;
            if (currentTarget != null &&
                currentTarget.GetComponent<UnitController>().GetUnitState() != UnitController.state.DEATH) 
                currentTarget.GetComponent<UnitController>().ExternalDamage(1);
        }
    }

    public void ReassignTarget(GameObject target)
    {
        currentTarget = target;
    }

    public void RemoveHomer(GameObject homer)
    {
        homingProjectiles.Remove(homer);
    }

    void UpdateHomers()
    {
        //Debug.LogError("Assigning new target");
        if (currentTarget == null || currentTarget.GetComponent<UnitController>().GetUnitState() == UnitController.state.DEATH)
        {
            //Debug.LogWarning("Dead/no target");
            foreach (GameObject homer in homingProjectiles)
            {
                homer.GetComponent<HomingProjectile>().AssignNewTarget(null);
            }
        }
        else
        {
            //Debug.LogWarning("Live target");
            foreach (GameObject homer in homingProjectiles)
            {
                homer.GetComponent<HomingProjectile>().AssignNewTarget(currentTarget);
            }
        }
    }

    void UpdateBolt()
    {
        // Is the target dead and we have no new target?
        if (currentTarget == null || currentTarget.GetComponent<UnitController>().GetUnitState() == UnitController.state.DEATH)
        {
            //Debug.LogWarning("Bolt target dead");
            if (bolt != null)
            {
                //Debug.LogWarning("Clearing bolt");
                Destroy(bolt.gameObject);
                bolt = null;
            }
        }
        // Do we have a target, an existing bolt, and enough ammo?
        else if (bolt != null && ResourceManager.rm.CheckResourceCost(ammoConsumption))
        {
            //Debug.Log(bolt.transform.GetChild(1).transform.position);
            // Set start at barrel point
            bolt.transform.GetChild(0).transform.position = attackPoint.position;
            // Set end at target center
            bolt.transform.GetChild(1).transform.position = currentTarget.GetComponent<Collider>().bounds.center;
        }
        // Do we have a target and NOW enough ammo, but no existing bolt?
        else if (bolt == null && ResourceManager.rm.CheckResourceCost(ammoConsumption))
        {
            // Spawn in electric effect prefab
            bolt = Instantiate(projectilePrefab, attackPoint.position, attackPoint.rotation);
            // Set start at barrel point
            bolt.transform.GetChild(0).transform.position = attackPoint.position;
            // Set end at target center
            bolt.transform.GetChild(1).transform.position = currentTarget.GetComponent<Collider>().bounds.center;
        }
        // Do we have a target, but not enough ammo?
        else if (!ResourceManager.rm.CheckResourceCost(ammoConsumption))
        {
            //Debug.LogWarning("Not enough ammo");
            if (bolt != null)
            {
                //Debug.LogWarning("Clearing bolt");
                Destroy(bolt.gameObject);
                bolt = null;
            }
        }
    }

    public Transform GetAttackPoint()
    {
        return attackPoint;
    }

    public float GetProjSpeed()
    {
        return projectileSpeed;
    }

    public float GetFireRate()
    {
        return fireRate;
    }

    public AttackType GetAttackType()
    {
        return attackType;
    }
}
