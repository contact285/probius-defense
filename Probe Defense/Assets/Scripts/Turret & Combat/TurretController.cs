﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(AttackComponent))]
public class TurretController : MonoBehaviour
{
    Vector3 startingAngle;
    Transform barrelTip;
    float projectileSpeed;
    Vector3 targetVelocity;
    float timeToImpact;

    // A list of all detected targets
    List<GameObject> availableTargets = new List<GameObject>();

    // Boolean for tracking whether or not the turret is currently firing
    bool firing = false;

    // List defining what tags enemies of this turret should target have
    public List<string> foeTags;

    // Should this turret rotate to face its target?
    public bool orientToTarget = true;

    // What speed does this turret turn at?
    public float orientSpeed = 50f;

    // What speed does this turret reset its orientation at?
    public float reorientSpeed = 50f;

    // What degree of inaccuracy will the turret begin to fire? (0 makes this REALLY strict)
    public float errorThreshold = 1f;

    // MasterAudioController string IDs used to play sounds for targeting and attacking
    public string targetAudioID;
    public string attackAudioID;

    GameObject currentTarget = null;

    // The barrel tip and projectile speed are obtained from AttackComponent for proper orientation
    //  and target acquisition
    AttackComponent AC;

    // The attack type used for turret attacks. Necessary to determine if we need to lead the target or not
    AttackComponent.AttackType at;

    // Start is called before the first frame update
    private void Start()
    {
        AC = GetComponent<AttackComponent>();
        barrelTip = AC.GetAttackPoint();
        projectileSpeed = AC.GetProjSpeed();
        startingAngle = transform.eulerAngles;
        at = AC.GetAttackType();
    }

    // Update is called once per frame
    private void Update()
    {
        // If a target in range has been acquired, orient to it (if needbe) and check if it can be shot
        if (currentTarget != null)
        {
            if (currentTarget.GetComponent<UnitController>().GetUnitState() != UnitController.state.DEATH)
            {
                // Orient turret if needbe to attack
                if (orientToTarget == true)
                {
                    var move = MoveTurret();

                    // if the target is within firing range, i.e. can be shot, rotate and fire when ready
                    if (move != Vector3.zero)
                    {
                        // Rotate the turret to its target over time
                        transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(move), Time.deltaTime * orientSpeed);

                        // if current rotation is within errorThreshold of on-target, start firing
                        if (firing == false && Quaternion.Angle(transform.rotation, Quaternion.LookRotation(move)) < errorThreshold)
                        {
                            //Debug.Log("Starting attack");
                            InvokeRepeating("Fire", 0f, AC.GetFireRate());
                            firing = true;
                        }
                    }
                    // Something is within targeting range, but cannot be hit (too fast or too far given proj speed/gravity)
                    else
                    {
                        //Debug.Log("Can't hit target");
                        firing = false;
                        CancelInvoke("Fire");
                    }
                }
                // Otherwise, just attack (since we have a non-null target)
                else
                {
                    InvokeRepeating("Fire", 0f, AC.GetFireRate());
                    firing = true;
                }
            }

            // Target is dead (or frame rate drop?), so remove it from the list and pick a new one
            else
            {
                RemoveTarget(currentTarget);
                SelectNewTarget();
            }
        }

        // No current target (died or destroyed, and is null)
        else
        {
            //Debug.Log("Resetting");
            // If we still have targets available, pick a new one
            if (availableTargets.Count > 0)
            {
                RemoveTarget(currentTarget);
                SelectNewTarget();
            }

            // Otherwise, remove our non-existent current target from the available list and move on
            else
            {
                //RemoveTarget(currentTarget);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(startingAngle), Time.deltaTime * reorientSpeed);
                firing = false;
                CancelInvoke("Fire");
            }
        }
    }

    private void Fire()
    {
        //Debug.Log("Firing");

        // If the target is not null and is alive, shoot it
        if (currentTarget != null && currentTarget.GetComponent<UnitController>().GetUnitState() != UnitController.state.DEATH)
        {
            var LOSPoint = GetLineOfSight(currentTarget.transform);
            if (LOSPoint != Vector3.zero)
            {
                if (AC.GetAttackType() == AttackComponent.AttackType.BULLET) AC.Attack(currentTarget, LOSPoint);
                else AC.Attack(currentTarget);
                // Move this to attack component?
                MasterAudioController.mac.PlaySfxByID(AC.GetAttackPoint().position, attackAudioID);
            }
        }
        // If we killed the target from firing (or something else did) pick a new target after removing it from the list
        else
        {
            RemoveTarget(currentTarget);
            SelectNewTarget();
        }
    }

    private Vector3 MoveTurret()
    {
        // TODO: conditionals for special turrets??
        //Debug.DrawLine(barrelTip.position, currentTarget.GetComponent<Collider>().bounds.center, Color.green);
        return currentTarget.GetComponent<Collider>().bounds.center - transform.position;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (foeTags.Contains(other.tag))
        {
            //Debug.Log("Enemy Detected");
            AddTarget(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (foeTags.Contains(other.tag))
        {
            //Debug.Log("Detected Enemy Left Range");
            RemoveTarget(other.gameObject);
        }
    }

    private void AddTarget(GameObject target)
    {
        availableTargets.Add(target);
        if (currentTarget == null) SelectNewTarget();
    }

    private void RemoveTarget(GameObject target)
    {
        Debug.Log("Removing target");
        availableTargets.Remove(target);
    }

    // Currently, this target selection should only choose a new target if one is not already selected.
    // That is, if a target is selected and a new one comes even closer, it won't be selected until the current
    // is dead or leaves range.
    private void SelectNewTarget()
    {
        //Debug.Log("Clearing null targets");
        ClearNullTargets();

        if (availableTargets.Count > 1)
        {
            Debug.Log("Choosing closest available target");
            float minDistance = Mathf.Infinity;
            GameObject newTarget = availableTargets[0];

            foreach(GameObject t in availableTargets)
            {
                // if distance to next available target is smaller than current closest, 
                // make that the newly-selected target
                var distance = Vector3.Distance(transform.position, t.transform.position);

     

                if (distance < minDistance)
                {
                    //Debug.Log("Better target found at " + distance + " meters distance.");
                    newTarget = t;
                    minDistance = distance;
                }
            }
            currentTarget = newTarget;
            AC.ReassignTarget(currentTarget);
            MasterAudioController.mac.PlaySfxByID(transform.position, targetAudioID);
        }
        else if (availableTargets.Count == 1)
        {
            //Debug.Log("Selecting 1 of 1 targets");
            currentTarget = availableTargets[0];
            AC.ReassignTarget(currentTarget);
            MasterAudioController.mac.PlaySfxByID(transform.position, targetAudioID);
        }
        else
        {
            //Debug.Log("No targets available.");
            currentTarget = null;
            AC.ReassignTarget(null);
        }
        firing = false;
        CancelInvoke("Fire");
    }

    /// <summary>
    /// Clears the current list of available targets of null instances, in the case
    /// that they have been destroyed by something other than this unit.
    /// </summary>
    private void ClearNullTargets()
    {
        List<GameObject> newTargets = new List<GameObject>();
        foreach (GameObject obj in availableTargets)
        {
            if (obj != null) newTargets.Add(obj);
        }
        availableTargets = newTargets;
    }

    private Vector3 GetLineOfSight(Transform target)
    {
        // Make sure we have a line-of-sight to this target, otherwise we don't consider it right now

        RaycastHit hit;
        // Define layers we're allowed to hit or go through (can check through units, allied or otherwise, but cannot aim through static objects/buildings/player)
        // Raycast from AttackComponent's start point to target's center (doesn't allow hitting large targets peeking around corners) up to the distance
        // If the target is hit by the raycast and isn't blocked by things we can't shoot through (walls, terrain, etc), consider it for targeting
        // Otherwise, ignore it

        int layerMask = 0;
        int friendlyDetectionLayer = 1 << 13;
        int friendlyLayer = 1 << 15;
        int enemyDetectionLayer = 1 << 10;
        int enemyLayer = 1 << 12;

        // If this is on an enemy layer
        if (gameObject.layer == 10 || gameObject.layer == 12)
        {
            // FriendlyCollision layer
            layerMask = enemyLayer | enemyDetectionLayer | friendlyDetectionLayer;
        }

        // If this is on a friendly layer
        else if (gameObject.layer == 13 || gameObject.layer == 15)
        {
            // EnemyCollision layer
            layerMask = friendlyLayer | friendlyDetectionLayer | enemyDetectionLayer;
        }

        // invert mask so we ONLY hit the selected layers
        layerMask = ~layerMask;

        Physics.Raycast(AC.GetAttackPoint().position, (AC.GetAttackPoint().forward), out hit, 9999, layerMask);
        if (hit.collider != null)
        {
            Debug.DrawLine(AC.GetAttackPoint().position, hit.point, Color.magenta, 0.5f);
            Debug.Log(hit.collider.transform.name);
            Debug.Log(hit.collider.transform == target);
            if (hit.collider.transform == target)
            {
                Debug.Log("LOS to target available");
                return hit.point;
            }
            else return Vector3.zero;
        }
        else return Vector3.zero;
    }
}
