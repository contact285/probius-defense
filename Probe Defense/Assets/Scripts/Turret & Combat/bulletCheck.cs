﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletCheck : MonoBehaviour {

    public float timeToLive;
    List<string> targets = new List<string>();

	// Use this for initialization
	void Start () {
        Destroy(this.gameObject, timeToLive);
	}
	
	// Update is called once per frame
	void Update () {
	}

    public void AssignTargetList(List<string> newTargets)
    {
        targets = newTargets;
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log(other.gameObject.name);
        if (targets.Contains(other.tag))
        {
            Debug.LogWarning("Target hit");
            
            Destroy(this.gameObject);
            
        }
        else
        {
            //Debug.Log(other.name);
        }
        //else if (projectile == false)
        //{
        //    GetComponent<Rigidbody>().velocity = Vector3.zero;
        //}
    }

    private void OnDestroy()
    {
        // are we calling this because the scene is closing, or is it because we're hitting something/fizzling?
        // TODO: address difference between impact/fizzle and when scenes change or game ends
        //Debug.LogWarning("I'M DYING OH NO");
        MasterAudioController.mac.PlaySfxByID(transform.position, "crawlerBulletImpact");
    }
}
