﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class AnyButtonContinue : MonoBehaviour
{
    InputAction cont = null;

    public GameObject saveFileHolder;
    public SaveManager sm;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        cont = new InputAction(binding: "/*/<button>");
        //cont.onPerformed += (InputAction, control) => Continue();
        cont.started += ctx => { Continue(); };
        cont.Enable();
    }

    private void OnDisable()
    {
        cont.Disable();
        cont = null;
    }

    void Continue()
    {
        //Debug.Log("Any button was pressed!");
        MainMenuManager.mmm.ToggleUIElement(saveFileHolder);
        sm.LoadLevels();
    }
}
