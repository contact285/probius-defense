﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsManager : MonoBehaviour
{
    public static SettingsManager sm { get; private set; }

    private void Awake()
    {
        if (sm != null)
        {
            Destroy(gameObject);
        }
        else
        {
            sm = this; 
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (!PlayerPrefs.HasKey("QualityPreset"))
        {
            Debug.Log("No quality preset playerpref found.");
            PlayerPrefs.SetString("QualityPreset", "Potato");
            ActivateQualityLevel("Potato");
            MainMenuManager.mmm.InitGraphicSettingsButtons(false);
        }
        else
        {
            var loadQuality = PlayerPrefs.GetString("QualityPreset");
            Debug.Log("Quality preset is: " + loadQuality);
            ActivateQualityLevel(loadQuality);
            if (loadQuality == "Potato") MainMenuManager.mmm.InitGraphicSettingsButtons(false);
            else MainMenuManager.mmm.InitGraphicSettingsButtons(true);

        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ActivateQualityLevel(string qualityName)
    {
        string[] qualityNameArray = QualitySettings.names;
        for (int i=0; i<qualityNameArray.Length; i++)
        {
            if (qualityNameArray[i] == qualityName)
            {
                Debug.Log("Setting quality \"" + qualityName + "\"");
                // pass false to not enable expensive settings like anti-aliasing (TODO: prompt restart for expensive settings- advanced!)
                QualitySettings.SetQualityLevel(i, true);
            }
        }
    }
}
