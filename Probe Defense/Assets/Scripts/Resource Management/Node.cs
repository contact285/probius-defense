﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class Node : MonoBehaviour
{
    [SerializeField]
    string resourceName = "";
    [SerializeField]
    int finiteResourceCount = 0, resourceStackCount = 0;
    [SerializeField]
    float productionRate = 0;

    [SerializeField]
    bool isFinite = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public int Harvest()
    {
        if (isFinite == true)
        {
            if (finiteResourceCount > resourceStackCount)
            {
                finiteResourceCount -= resourceStackCount;
                return resourceStackCount;
            }
            else if (finiteResourceCount > 0)
            {
                var last = finiteResourceCount;
                finiteResourceCount = 0;
                return last;
            }
            else
            {
                Debug.Log("No resources, giving 0");
                return 0;
            }
        }
        else return resourceStackCount;
    }

    public string GetResourceName()
    {
        return resourceName;
    }

    public int GetResourceStackCount()
    {
        return resourceStackCount;
    }

    public float GetProductionRate()
    {
        return productionRate;
    }

    public float GetEffectiveRate()
    {
        return resourceStackCount / productionRate;
    }

    public bool ResourcesRemaining()
    {
        // Either this resource is infinite,
        //  or we have resources remaining to harvest
        return (!isFinite || finiteResourceCount > 0);
    }


}
