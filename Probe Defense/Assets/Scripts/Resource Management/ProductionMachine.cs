﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProductionMachine : MonoBehaviour
{
    public string faction = "";
    // These fields need to be serialized for placing/setting enemy resources
    [SerializeField] Node node;
    [SerializeField] bool producing = false;
    float productionRate = 0;

    // Start is called before the first frame update
    void Start()
    {
        if (faction == "")
        {
            Debug.LogError("This machine, " + gameObject.name + ", does not have a valid faction!");
        }
        else if (producing == true)
        {
            productionRate = node.GetProductionRate();
            InvokeRepeating("Produce", productionRate, productionRate);

            // if this machine belongs to the player, 
            //  add it to the resource manager for resource rate checks
            ResourceManager.rm.AddNewProductionMachine(this);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetNode(Node placementNode)
    {
        //Debug.Log("Setting current node for production");
        node = placementNode;
        ToggleProduction(true);
        //StartCoroutine(Produce());
        productionRate = node.GetProductionRate();
        if (faction != "") InvokeRepeating("Produce", productionRate, productionRate);
    }

    void Produce()
    {
        if (producing == true && node != null)
        {
            // Produce the resource
            if (ResourceManager.rm != null && node.ResourcesRemaining() == true)
            {
                //ResourceManager.rm.AddResource(node.GetResourceName(), node.Harvest());
                AddResourceToFaction(node.GetResourceName(), node.Harvest());
            }
            else if (node.ResourcesRemaining() == false)
            {
                //Debug.Log("No more resources! Stopping production to save cpu");
                CancelInvoke();
                producing = false;
            }
        }
    }

    void AddResourceToFaction(string name, int count)
    {
        if (faction == "Player")
        {
            ResourceManager.rm.AddResource(name, count);
        }
        else
        {
            //Debug.Log("Producing for " + faction);
            EnemyResourcesManager.erm.AddResource(name, count, faction);
        }
    }

    public void ToggleProduction(bool onOff)
    {
        producing = onOff;
    }

    public bool GetProducing()
    {
        return producing;
    }

    public string GetNodeResourceName()
    {
        return node.GetResourceName();
    }

    public float GetNodeResourceRate()
    {
        return node.GetEffectiveRate();
    }

    private void OnDisable()
    {
        ToggleProduction(false);
        ResourceManager.rm.RemoveProductionMachine(this);
    }
}
