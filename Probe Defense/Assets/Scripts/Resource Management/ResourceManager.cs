﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ResourceManager : MonoBehaviour
{
    public static ResourceManager rm { get; private set; }
    public ResourceInitializationData rd;
    public GameObject ResourceUI;
    public GameObject ResourcePoolPrefab;

    // Lists of production and processing machines used to check what current
    // resource production/consumption rates are
    List<ProductionMachine> activeProducers = new List<ProductionMachine>();
    List<ProcessingMachine> activeProcessors = new List<ProcessingMachine>();

    Dictionary<string, float> resourceRates = new Dictionary<string, float>();

    ResourcePools RP;

    private void Awake()
    {
        // If for some reason there is more than 1 ResourceManager in the scene,
        //  destroy the old one to avoid complications
        if (rm != null)
        {
            Destroy(rm.gameObject);
            rm = null;
        }
        rm = this;

        RP = new ResourcePools();
        // Create a new UI object and Resource Pool for each resource in the initialization data

        Debug.Log("Initializing resource pools from Init Data.");
        foreach (ResourceData data in rd.GetData())
        {
            // Make a new UI object
            var newPoolUI = Instantiate(ResourcePoolPrefab, ResourceUI.transform);
            // Set UI sprite to data sprite
            newPoolUI.transform.GetChild(0).GetComponent<Image>().sprite = data.GetSprite();
            // Set UI Name text to data name
            // Set UI Count text to data starting count
            //newPoolUI.transform.GetChild(1).GetComponent<TMP_Text>().text = data.GetName() + ": " + data.GetResourceStartingCount();
            newPoolUI.transform.GetChild(1).GetComponent<TMP_Text>().text = data.GetResourceStartingCount().ToString("n2");
            // Make a new resource pool for the resource
            RP.CreateNewResourcePool(data.GetName(), data.GetResourceStartingCount());
        }

        //resourceRates = new List<float>(new float[RP.GetResourceCounts().Count]);
        foreach (string s in RP.GetResourceNames())
        {
            resourceRates.Add(s, 0);
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        UpdateResourcePoolUI();
    }

    private void LateUpdate()
    {
        
    }

    private void OnDestroy()
    {
        CancelInvoke();
    }

    public void AddResource(string resourceName, int resourceCount)
    {
        RP.AddResourceToPool(resourceName, resourceCount);
    }

    public void ConsumeFromResourceCosts(List<ResourceCost> costs)
    {
        foreach(ResourceCost cost in costs)
        {
            RP.RemoveResourceFromPool(cost.resourceName, cost.resourceCount);
        }
    }

    public void AddNewProductionMachine(ProductionMachine pm)
    {
        //Debug.Log("Adding production machine");
        activeProducers.Add(pm);

        // Add the produced resource to the current rate
        resourceRates[pm.GetNodeResourceName()] += pm.GetNodeResourceRate();
    }

    public void RemoveProductionMachine(ProductionMachine pm)
    {
        activeProducers.Remove(pm);

        // Subtract the previously produced resource from the current rate
        resourceRates[pm.GetNodeResourceName()] -= pm.GetNodeResourceRate();
    }

    public void AddNewProcessingMachine(ProcessingMachine pm)
    {
        //Debug.Log("Adding processing machine");
        activeProcessors.Add(pm);

        // For each input item processed,
        //  subtract the consumption rate from the current rate
        foreach(ResourceCost rc in pm.GetInput())
        {
            var effectiveRate = rc.resourceCount / pm.GetProductionRate();
            resourceRates[rc.resourceName] -= effectiveRate;
        }

        // For each output item processed,
        //  add the production rate to the current rate
        foreach(ResourceCost rc in pm.GetOutput())
        {
            var effectiveRate = rc.resourceCount / pm.GetProductionRate();
            resourceRates[rc.resourceName] += effectiveRate;
        }
    }

    public void RemoveProcessingMachine(ProcessingMachine pm)
    {
        activeProcessors.Remove(pm);

        // For each input item processed,
        //  add the consumption rate back to the current rate
        foreach (ResourceCost rc in pm.GetInput())
        {
            var effectiveRate = rc.resourceCount / pm.GetProductionRate();
            resourceRates[rc.resourceName] += effectiveRate;
        }

        // For each output item processed,
        //  subtract the production rate back from the current rate
        foreach (ResourceCost rc in pm.GetOutput())
        {
            var effectiveRate = rc.resourceCount / pm.GetProductionRate();
            resourceRates[rc.resourceName] -= effectiveRate;
        }
    }

    void UpdateResourcePoolUI()
    {
        var resourceNames = RP.GetResourceNames();
        for (int i=0; i<ResourceUI.transform.childCount; i++)
        {
            var poolText = ResourceUI.transform.GetChild(i).GetChild(1).GetComponent<TMP_Text>();
            //poolText.text = resourceNames[i] + ": " + RP.GetResourceCountByName(resourceNames[i]);

            var rate = resourceRates[RP.GetResourceNames()[i]];
            var rateSign = (rate >= 0 ? " (+" : " (");

            poolText.text = RP.GetResourceCountByName(resourceNames[i]).ToString() 
                + rateSign + rate.ToString("n2") + ")";
        }
    }

    public bool CheckResourceCost(List<ResourceCost> costs)
    {
        // Assume we have the resources
        bool haveResources = true;

        // Check to be sure
        foreach (ResourceCost cost in costs)
        {
            // If at any point we find a cost we can't supply, change response to false
            if (RP.GetResourceCountByName(cost.resourceName) < cost.resourceCount)
            {
                //Debug.Log("Do not have the resources, don't build");
                haveResources = false;
            }
        }
        return haveResources;
    }

    public Sprite GetSpriteFromResourcePool(string name)
    {
        var resource = rd.GetResourceByName(name);
        if (resource != null)
        {
            return resource.GetSprite();
        }
        else
        {
            Debug.LogError("Could not find a sprite for resource \"" + name + "\", returning null sprite");
            return null;
        }
    }
}
