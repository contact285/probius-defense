﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ResourceData
{
    public string resourceName;
    public Sprite resourceSprite;
    public int resourceStartingCount;

    public Sprite GetSprite()
    {
        return resourceSprite;
    }

    public string GetName()
    {
        return resourceName;
    }

    public int GetResourceStartingCount()
    {
        return resourceStartingCount;
    }
}

[CreateAssetMenu(fileName = "ResourceData", menuName = "ScriptableObjects/ResourceInitializationData", order = 1)]
public class ResourceInitializationData : ScriptableObject
{
    public List<ResourceData> data = new List<ResourceData>();

    public List<ResourceData> GetData()
    {
        return data;
    }

    public ResourceData GetResourceByName(string name)
    {
        ResourceData returnData = null;

        foreach(ResourceData rd in data)
        {
            if (rd.GetName() == name) returnData = rd;
        }

        return returnData;
    }
}
