﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyResourcesManager : MonoBehaviour
{
    public static EnemyResourcesManager erm { get; private set; }

    // These two lists MUST have the same size!
    public List<ResourceInitializationData> rdList;
    public List<string> enemyFactions;
    Dictionary<string, ResourcePools> rpDict = new Dictionary<string, ResourcePools>();

    private void Awake()
    {
        // There should only ever be 1 ERM in the scene.
        // If there's another, destroy the OLD one.
        if (erm != null)
        {
            Destroy(erm.gameObject);
            erm = null;
        }
        else
        {
            erm = this;
        }

        for (int i = 0; i < enemyFactions.Count; i++)
        {
            Debug.LogWarning(enemyFactions[i]);
            var rp = new ResourcePools();
            foreach (ResourceData data in rdList[i].GetData())
            {
                rp.CreateNewResourcePool(data.GetName(), data.GetResourceStartingCount());
            }
            rpDict.Add(enemyFactions[i], rp);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    bool validateFaction(string faction)
    {
        return rpDict.ContainsKey(faction);
    }

    public void AddResource(string resourceName, int resourceCount, string faction)
    {
        if (validateFaction(faction) == true)
        {
            rpDict[faction].AddResourceToPool(resourceName, resourceCount);
            //Debug.Log("Added " + resourceCount + " " + resourceName + " to faction " + faction);
            //Debug.Log("There are now " + rpDict[faction].GetResourceCountByName(resourceName) + " " + resourceName);
        }
        else Debug.LogError("No faction " + faction + " currently exists. This is a bug!");
    }

    public void ConsumeFromResourceCosts(List<ResourceCost> costs, string faction)
    {
        if (validateFaction(faction) == true)
        {
            foreach (ResourceCost cost in costs)
            {
                rpDict[faction].RemoveResourceFromPool(cost.resourceName, cost.resourceCount);
            }
        }
        else Debug.LogError("No faction " + faction + " currently exists. This is a bug!");
    }

    public bool CheckResourceCost(List<ResourceCost> costs, string faction)
    {
        if (validateFaction(faction) == true)
        {
            // Assume we have the resources
            bool haveResources = true;

            // Check to be sure
            foreach (ResourceCost cost in costs)
            {
                // If at any point we find a cost we can't supply, change response to false
                if (rpDict[faction].GetResourceCountByName(cost.resourceName) < cost.resourceCount)
                {
                    //Debug.Log("Do not have the resources, don't build");
                    haveResources = false;
                }
            }
            return haveResources;
        }
        else
        {
            Debug.LogError("No faction " + faction + " currently exists. This is a bug!");
            return false;
        }
    }
}
