﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ProcessingRecipe
{
    public List<ResourceCost> inputResources;
    public List<ResourceCost> outputResources;
}

public class ProcessingMachine : MonoBehaviour
{
    public string faction = "";
    public ProcessingRecipe recipe;

    [SerializeField]
    float productionRate;
    [SerializeField]
    bool producing = false;

    // Start is called before the first frame update
    void Start()
    {
        if (faction == "")
        {
            Debug.LogError("This machine, " + gameObject.name + ", does not have a valid faction!");
        }
        // if this machine belongs to the player, 
        //  add it to the resource manager for resource rate checks
        ResourceManager.rm.AddNewProcessingMachine(this);
    }

    // Update is called once per frame
    void Update()
    {
        if (producing == false)
        {         
            if (faction == "Player")
            {
                // Make sure the RM actually exists and whether or not it has the resources required to produce
                if (ResourceManager.rm != null && ResourceManager.rm.CheckResourceCost(recipe.inputResources))
                {
                    ToggleProduction(true);
                    StartCoroutine(Produce());
                }
            }
            else
            {
                if (EnemyResourcesManager.erm != null && EnemyResourcesManager.erm.CheckResourceCost(recipe.inputResources, faction))
                {
                    ToggleProduction(true);
                    StartCoroutine(Produce());
                }
            }
        }
    }

    IEnumerator Produce()
    {
        // Consume first, to avoid race conditions
        ConsumeResourceFromFaction();
        yield return new WaitForSeconds(productionRate);
        
        // THEN produce
        foreach (ResourceCost rc in recipe.outputResources)
        {
            AddResourceToFaction(rc.resourceName, rc.resourceCount);
        }

        // Adjusted to no longer automatically toggle production, if resource is available RIGHT now
        if (!ResourceManager.rm.CheckResourceCost(recipe.inputResources)) ToggleProduction(false);
        else StartCoroutine(Produce());
    }

    void ConsumeResourceFromFaction()
    {
        if (faction == "Player")
        {
            ResourceManager.rm.ConsumeFromResourceCosts(recipe.inputResources);
        }
        else
        {
            EnemyResourcesManager.erm.ConsumeFromResourceCosts(recipe.inputResources, faction);
        }
    }

    void AddResourceToFaction(string name, int count)
    {
        if (faction == "Player")
        {
            ResourceManager.rm.AddResource(name, count);
        }
        else
        {
            EnemyResourcesManager.erm.AddResource(name, count, faction);
        }
    }

    public void ToggleProduction(bool onOff)
    {
        //if (onOff == false) Debug.Log("no resource available, disabling");
        producing = onOff;
    }

    public bool GetProducing()
    {
        return producing;
    }

    public float GetProductionRate()
    {
        return productionRate;
    }

    public List<ResourceCost> GetInput()
    {
        return recipe.inputResources;
    }

    public List<ResourceCost> GetOutput()
    {
        return recipe.outputResources;
    }

    private void OnDisable()
    {
        ToggleProduction(false);
        ResourceManager.rm.RemoveProcessingMachine(this);
    }

}
