﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class ResourceCost
{
    public string resourceName;
    public int resourceCount;
}

[System.Serializable]
public class ResourcePools
{
    Dictionary<string, int> resources;

    public ResourcePools()
    {
        resources = new Dictionary<string, int>();
    }

    public int GetResourceCountByName(string name)
    {
        if (!resources.ContainsKey(name))
        {
            Debug.LogError("No resource \"" + name + "\" was found in the resource pool, returning 0 for resource count");
            return 0;
        }
        return resources[name];
    }

    public List<string> GetResourceNames()
    {
        return resources.Keys.ToList<string>();
    }

    public List<int> GetResourceCounts()
    {
        return resources.Values.ToList<int>();
    }

    public void CreateNewResourcePool(string name, int startingCount)
    {
        resources.Add(name, startingCount);
        //Debug.Log("Added new resource \" " + name + ",\" with a starting value of " + startingCount);
    }

    public void AddResourceToPool(string name, int count)
    {
        if (resources.ContainsKey(name) == true)
        {
            resources[name] += count;
            //Debug.Log("There were " + (resources[name]-count) + " " + name + " previously. There are now " + resources[name] + ".");
        }
        else
        {
            Debug.LogError("No resource \"" + name + "\" was found in the resource pool.");
        }
    }

    public void RemoveResourceFromPool(string name, int count)
    {
        if (resources.ContainsKey(name) == true)
        {
            resources[name] -= count;
            //Debug.Log("There were " + (resources[name]-count) + " " + name + " previously. There are now " + resources[name] + ".");
        }
        else
        {
            Debug.LogError("No resource \"" + name + "\" was found in the resource pool.");
        }
    }
}
