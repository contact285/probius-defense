﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(StaticBuildingUnitController))]
public class CoreDeath : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDestroy()
    {
        Debug.LogWarning(name + " has been destroyed! Trigger a core death event now!");
        InGameManager.im.CoreDeathTrigger(tag);
    }
}
