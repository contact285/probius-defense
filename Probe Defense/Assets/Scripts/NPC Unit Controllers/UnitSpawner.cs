﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitSpawner : MonoBehaviour
{
    public enum UnitType
    {
        CRAWLER
    }

    public UnitType unitType = UnitType.CRAWLER;
    public string faction;
    public List<ResourceCost> costs;
    public GameObject unitPrefab;
    public Transform spawnPoint;
    public float spawnRate;
    bool spawning = false;

    // Temporary- need to make core globally accessible
    public Transform target;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (spawning == false)
        {
            // Check if resources are available to spawn
            if (faction == "Player" && ResourceManager.rm.CheckResourceCost(costs))
            {
                // Yes? Consume resources and spawn a unit
                ResourceManager.rm.ConsumeFromResourceCosts(costs);
                StartCoroutine(SpawnUnit());
            }
            else if (faction != "Player" && EnemyResourcesManager.erm.CheckResourceCost(costs, faction))
            {
                EnemyResourcesManager.erm.ConsumeFromResourceCosts(costs, faction);
                StartCoroutine(SpawnUnit());
            }
        }
    }

    IEnumerator SpawnUnit()
    {
        //Debug.LogWarning("Spawning unit!");
        spawning = true;
        var newUnit = Instantiate(unitPrefab, spawnPoint.position, spawnPoint.rotation);
        newUnit.GetComponent<UnitController>().SetFinalTarget(target);
        yield return new WaitForSeconds(spawnRate);
        //Debug.LogWarning("Ready to spawn again.");
        spawning = false;
    }
}
