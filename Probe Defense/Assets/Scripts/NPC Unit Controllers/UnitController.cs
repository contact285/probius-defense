﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

abstract public class UnitController : MonoBehaviour
{
    // Inheritable state that controls what animation parameters are modified
    // Note: use state.Add(key, value) to add extra states for child classes via code (rather than manual)
    public enum state
    {
        IDLE,
        MOVING,
        ATTACKING,
        FLEEING,
        DEATH
    }

    protected NavMeshAgent NMA;

    // Current state of this unit
    [SerializeField]
    protected state currentState = state.IDLE;

    // Starting health of this unit
    public float startingHP = 100;

    // Current health of this unit
    protected float health = 0;

    // Time to wait until destroying this object, in seconds
    public float TimeToDie = 3.0f;

    // Does this unit ever attack?
    public bool canAttack = false;

    // Does this unit ever flee?
    public bool canFlee = false;

    // Is this unit invulnerable?
    public bool invuln = false;

    // If hit, how long does it take for this unit to recover?
    public float recoilTime = 0;

    protected List<GameObject> availableTargets = new List<GameObject>();
    public Transform finalTarget;
    protected GameObject currentTarget = null;
    protected AttackComponent AC;
    [SerializeField] protected List<string> foeTags = new List<string>();
    protected bool firing;

    // Start is called before the first frame update
    protected void Start()
    {
        NMA = GetComponent<NavMeshAgent>();
        if (NMA != null) NMA.enabled = true;
        health = startingHP;
        if (canAttack == true) AC = GetComponent<AttackComponent>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// Inheritable function that checks parameters to determine what the current state is.
    /// - IDLE: unit has no target location and is not attacking something
    /// - MOVING: unit has a target location but is not attacking something
    /// - ATTACKING: unit does not have a target location but is attacking something
    /// - DEATH: unit has been killed, and must reset its location/animations before being destroyed
    /// </summary>
    virtual protected void StateUpdate()
    {
        // Has the unit lost all of its health? Immediately transition to DEATH state, trigger the animation, and start a Destroy

        // Otherwise, does the unit's NMA have a current movement location? *Should* it have one?
        // If so, transition to the MOVING state and update animations as needed

        // Otherwise, is the unit within attack range (provided it can attack) and not busy travelling?
        // If so, transition to the ATTACKING state, update animations as needed, and perform this unit's attack

        // Otherwise, is the unit about to flee (provided it can flee) and not already doing so?
        // If so, transition to the FLEEING state, updated animations as needed, and move the NMA to a new target

        // Otherwise, this unit is in its idle state. Transition to the IDLE state and update animations as needed
    }

    /// <summary>
    /// Damages this unit. Can be negative for healing.
    /// </summary>
    /// <param name="damage"> The value of damage to be subtracted from this unit's
    ///  health. Negative values will heal the unit. </param>
    ///  <returns> Returns whether this instance of damage was deadly or not. </returns>
    virtual protected bool TakeDamage(int damage)
    {
        //Debug.Log("Taking damage");
        health -= damage;
        //Debug.Log(health);
        return CheckDeath();
    }

    /// <summary>
    /// Check to see if this unit has died.
    /// </summary>
    /// <returns> Bool if current health is at or below zero </returns>
    virtual protected bool CheckDeath()
    {
        return health <= 0;
    }

    virtual public state GetUnitState()
    {
        return currentState;
    }

    /// <summary>
    /// Called by external entities to damage this unit in the case of
    /// non-projectile-based damage.
    /// </summary>
    /// <param name="damage">Damage to deal to this unit</param>
    virtual public void ExternalDamage(int damage)
    {
        var hitDeathResult = TakeDamage(damage);
    }

    virtual protected void AddTarget(GameObject target)
    {
        //Debug.Log("Target detected");
        availableTargets.Add(target);
        if (currentTarget == null) SelectNewTarget();
    }

    virtual protected void RemoveTarget(GameObject target)
    {
        //Debug.Log("Removing target " + target);
        availableTargets.Remove(target);
    }

    // Currently, this target selection should only choose a new target if one is not already selected.
    // That is, if a target is selected and a new one comes even closer, it won't be selected until the current
    // is dead or leaves range.
    virtual protected void SelectNewTarget()
    {
        //Debug.Log("Clearing null targets");
        ClearNullTargets();

        if (availableTargets.Count > 1)
        {
            //Debug.Log("Choosing closest available target");
            float minDistance = Mathf.Infinity;
            GameObject newTarget = availableTargets[0];

            foreach (GameObject t in availableTargets)
            {
                // if distance to next available target is smaller than current closest, 
                // make that the newly-selected target
                var distance = Vector3.Distance(transform.position, t.transform.position);
                if (distance < minDistance)
                {
                    //Debug.Log("Better target found at " + distance + " meters distance.");
                    newTarget = t;
                    minDistance = distance;
                }
            }
            currentTarget = newTarget;
            AC.ReassignTarget(currentTarget);
        }
        else if (availableTargets.Count == 1)
        {
            //Debug.Log("Selecting 1 of 1 targets");
            currentTarget = availableTargets[0];
            AC.ReassignTarget(currentTarget);
        }
        else
        {
            //Debug.Log("No targets available.");
            currentTarget = null;
            AC.ReassignTarget(null);
        }
        //firing = false;
        //CancelInvoke("Fire");
    }

    /// <summary>
    /// Clears the current list of available targets of null instances, in the case
    /// that they have been destroyed by something other than this unit.
    /// </summary>
    private void ClearNullTargets()
    {
        List<GameObject> newTargets = new List<GameObject>();
        foreach(GameObject obj in availableTargets)
        {
            if (obj != null) newTargets.Add(obj);
        }
        availableTargets = newTargets;
    }
       
    virtual protected void CombatUpdate()
    {

    }

    virtual public void SetFinalTarget(Transform target)
    {
        finalTarget = target;
    }

    virtual public void SetFoeTags(List<string> foes)
    {
        foeTags = foes;
    }
}
