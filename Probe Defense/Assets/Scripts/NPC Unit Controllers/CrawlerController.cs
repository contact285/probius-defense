﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

/// <summary>
/// Crawler unit, capable of attacking something and fleeing.
/// Currently used for testing.
/// </summary>
public class CrawlerController : UnitController
{
    Animator anim;
    Rigidbody rb;
    public Vector3 centerOfMassOffset;

    // Start is called before the first frame update
    void Start()
    {
        base.Start();
        anim = GetComponentInChildren<Animator>();
        rb = GetComponent<Rigidbody>();
        rb.centerOfMass = centerOfMassOffset;
    }

    // Update is called once per frame
    void Update()
    {
        // Any unit that has died or is in the process should not need to update anymore
        // (projectiles it has fired, when applicable, should update on their own)
        if (currentState != state.DEATH) StateUpdate();
    }

    protected override void StateUpdate()
    {
        if (currentState == state.MOVING)
        {
            anim.SetBool("Run Forward", true);
            if (NMA.hasPath == true && NMA.remainingDistance <= NMA.stoppingDistance && finalTarget != null)
            {
                NMA.ResetPath();

                // Should we be attacking? if not, go idle
                currentState = state.IDLE;
            }
        }

        // We only enter ATTACKING state if we encounter a defensive building
        if (currentState == state.ATTACKING)
        {
            
            //if (NMA.hasPath == true)
            //{
            //    //Debug.LogWarning("Resetting path of " + this.gameObject.name);
            //    NMA.ResetPath();
            //}
            if (currentTarget != null)
            {
                // If we have line-of-sight to target, we can stop moving and shoot. Otherwise, we should move to the target until we do.
                // TODO: force unit to follow its target if it leaves range (will this be an issue if units attack ANY unit in their (mutual) range?)
                if (GetLineOfSight(currentTarget.transform) == true)
                {
                    if (NMA.hasPath == true)
                    {
                        //Debug.Log("LOS found, moving to target.");
                        NMA.ResetPath();
                        anim.SetBool("Run Forward", false);
                    }
                    Vector3 direction = (currentTarget.transform.position - transform.position).normalized;
                    Quaternion lookRotation = Quaternion.LookRotation(direction);
                    transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 30f);
                    CombatUpdate();
                }
                else
                {
                    if (NMA.destination != currentTarget.transform.position)
                    {
                        Debug.Log("Moving to target to gain LOS");
                        //Debug.Log(NMA.destination + " " + currentTarget.transform.position);
                        NMA.SetDestination(currentTarget.transform.position);
                    }
                }
            }
            // If we're in the attacking state but our target is suddenly null, it must have died (or otherwise disappeared)
            // Try to find another target, and if that's not successful, move to the idle state (if our final target still
            //  exists, the idle state will handle that)
            else
            {
                SelectNewTarget();
                if (currentTarget == null) currentState = state.IDLE;
            }
            
        }

        else if (currentState == state.IDLE)
        {
            anim.SetBool("Run Forward", false);
            if (finalTarget != null)
            { 
                NMA.SetDestination(finalTarget.position);
                currentState = state.MOVING;
            }
        }

        else if (currentState == state.DEATH)
        {
            anim.SetBool("Run Forward", false);
            CancelInvoke("Attack");
        }


        //if (NMA.hasPath == false)
        //{
        //    Debug.Log("No destination");
        //}
        //else
        //{
        //    Debug.Log("Has destination");
        //}
    }

    // TODO: keep track of a list of available targets (similar to turret controller) and target others if possible
    //private void OnTriggerStay(Collider other)
    //{
    //    //Debug.Log(other.tag);
    //    if (other.tag == "Defense" && currentState != state.ATTACKING)
    //    {
    //        currentState = state.ATTACKING;
    //        currentTarget = other.gameObject;
    //        //InvokeRepeating("Attack", 0, ac.GetFireRate());
    //        StartCoroutine(Attack());
    //    }
    //}

    private void OnTriggerEnter(Collider other)
    {
        if (currentState != state.DEATH)
        {
            if (foeTags.Contains(other.tag) && other.GetComponent<UnitController>().GetUnitState() != state.DEATH)
            {
                //Debug.Log("Enemy Detected");
                AddTarget(other.gameObject);
                // Enter attacking state, assuming the new target isn't null
                if (currentState != state.ATTACKING)
                {
                    currentState = state.ATTACKING;
                    // Play sound indicating new target
                    MasterAudioController.mac.PlaySfxByID(transform.position, "crawlerTarget");
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (currentState != state.DEATH)
        {
            if (foeTags.Contains(other.tag))
            {
                //Debug.Log("Detected Enemy Left Range");
                RemoveTarget(other.gameObject);
            }
        }
    }

    IEnumerator Attack()
    {
        //Debug.Log("Attack");
        if (currentTarget != null)
        {
            anim.SetTrigger("Long Shoot Attack");
            yield return new WaitForSeconds(AC.GetFireRate()/2 - 0.01f);
            // There are some occurrences where the target might die between attacks (due to other units)
            if (currentTarget != null)
            {
                AC.Attack(currentTarget);
                MasterAudioController.mac.PlaySfxByID(AC.GetAttackPoint().position, "crawlerBullet");
            }
            yield return new WaitForSeconds(AC.GetFireRate()/2);
            StartCoroutine(Attack());
        }
        else yield return new WaitForEndOfFrame();
    }

    override protected void CombatUpdate()
    {
        //Debug.Log("Combat update");
        //Debug.LogWarning(gameObject.name);
        // If a target in range has been acquired, check to see if we should attack it
        if (currentTarget != null)
        {
            //Debug.Log(currentTarget.GetComponent<UnitController>().GetUnitState() == state.DEATH);
            // If the target isn't dead, attack!
            if (currentTarget.GetComponent<UnitController>().GetUnitState() != state.DEATH)
            {
                //Debug.Log("Target is not dead");
                if (firing == false)
                {
                    //Debug.Log("Beginning my attack!");
                    StartCoroutine(Attack());
                    firing = true;
                }
            }

            // Target is dead (or frame rate drop?), so remove it from the list and pick a new one
            else
            {
                Debug.Log("Current target is dead, checking for more");
                RemoveTarget(currentTarget);
                SelectNewTarget();
            }
        }

        // No current target (died or destroyed)
        else
        {
            // If we still have targets available, pick a new one
            if (availableTargets.Count > 0)
            {
                Debug.Log("More targets! Continuing to attack");
                RemoveTarget(currentTarget);
                SelectNewTarget();
            }

            // Otherwise, remove our non-existent current target from the available list and move on
            else
            {
                Debug.LogWarning("No targets in range for " + gameObject.name + " , moving on");
                RemoveTarget(currentTarget);
                firing = false;
                //CancelInvoke("Attack");
                // Exit the attacking state, since we have no available target
                // (assume that we can idle and figure out what to do from there)
                currentState = state.IDLE;
            }
        }
    }

    public override void ExternalDamage(int damage)
    {
        // Stop navmeshagent for a time, then resume

        var hitDeathResult = TakeDamage(damage);

        // If that instance caused death, transition to the DEATH state
        if (hitDeathResult == true)
        {
            //Debug.Log("Dead!");
            currentState = state.DEATH;
            NMA.ResetPath();
            NMA.enabled = false;
            anim.SetTrigger("Die");
            anim.SetBool("Run Forward", false);
            MasterAudioController.mac.PlaySfxByID(transform.position, "crawlerDie");
            //GetComponent<Collider>().enabled = false;
            StopAllCoroutines();
            //Debug.Log("I'm dying!");
            Destroy(gameObject, TimeToDie);
            //foreach(Collider c in GetComponentsInChildren<Collider>())
            //{
            //    c.enabled = false;
            //}
        }
        else
        {
            StartCoroutine(DamageRecoil());
        }
    }

    IEnumerator DamageRecoil()
    {
        if (NMA.isStopped == false)
        {
            anim.SetTrigger("Take Damage");
            MasterAudioController.mac.PlaySfxByID(transform.position, "crawlerHit");
            NMA.isStopped = true;
            yield return new WaitForSeconds(recoilTime);
            if (NMA.enabled == true) NMA.isStopped = false;
        }
    }

    public bool GetLineOfSight(Transform target)
    {
        // Make sure we have a line-of-sight to this target, otherwise we don't consider it right now

        RaycastHit hit;
        // Define layers we're allowed to hit or go through (can check through units, allied or otherwise, but cannot aim through static objects/buildings/player)
        // Raycast from AttackComponent's start point to target's center (doesn't allow hitting large targets peeking around corners) up to the distance
        // If the target is hit by the raycast and isn't blocked by things we can't shoot through (walls, terrain, etc), consider it for targeting
        // Otherwise, ignore it

        int layerMask = 0;
        int friendlyDetectionLayer = 1 << 13;
        int friendlyLayer = 1 << 15;
        int enemyDetectionLayer = 1 << 10;
        int enemyLayer = 1 << 12;

        // If this is on an enemy layer
        if (gameObject.layer == 10 || gameObject.layer == 12)
        {
            // FriendlyCollision layer
            layerMask = enemyLayer | enemyDetectionLayer | friendlyDetectionLayer;
        }

        // If this is on a friendly layer
        else if (gameObject.layer == 13 || gameObject.layer == 15)
        {
            // EnemyCollision layer
            layerMask = friendlyLayer | friendlyDetectionLayer | enemyDetectionLayer;
        }

        // invert mask so we ONLY hit the selected layers
        layerMask = ~layerMask;

        Physics.Raycast(transform.GetComponent<Rigidbody>().worldCenterOfMass, 
            (currentTarget.GetComponent<Rigidbody>().worldCenterOfMass - transform.GetComponent<Rigidbody>().worldCenterOfMass), 
            out hit, 9999, layerMask);
        Debug.DrawRay(transform.GetComponent<Rigidbody>().worldCenterOfMass,
            (currentTarget.GetComponent<Rigidbody>().worldCenterOfMass - transform.GetComponent<Rigidbody>().worldCenterOfMass),
            Color.red);
        if (hit.collider != null)
        {
            Debug.DrawLine(AC.GetAttackPoint().position, hit.point, Color.magenta);
            //Debug.Log(hit.collider.transform.name);
            //Debug.Log(hit.collider.transform == target);
            if (hit.collider.transform == target)
            {
                //Debug.Log("LOS to target available");
                return true;
            }
            else return false;
        }
        else
        {
            //Debug.LogWarning("Nothing hit by raycast");
            return false;
        }
    }

}
